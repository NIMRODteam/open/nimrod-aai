!-------------------------------------------------------------------------------
!< routines for type conversion
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> module containing routines for type conversion
!-------------------------------------------------------------------------------
MODULE convert_type
  USE local
  USE, INTRINSIC :: IEEE_ARITHMETIC, ONLY: IEEE_Value, IEEE_SIGNALING_NAN
  IMPLICIT NONE

CONTAINS

!-------------------------------------------------------------------------------
!> convert to a REAL(r8) type, return NaN if conversion fails
!-------------------------------------------------------------------------------
  ELEMENTAL FUNCTION convert_to_real_r8(input) RESULT(output)
    IMPLICIT NONE

    !> input variable
    CLASS(*), INTENT(IN) :: input

    REAL(r8) :: output

    SELECT TYPE (input)
    TYPE IS (INTEGER(i4))
      output=REAL(input,r8)
    TYPE IS (INTEGER(i8))
      output=REAL(input,r8)
    TYPE IS (REAL(r4))
      output=REAL(input,r8)
    TYPE IS (REAL(r8))
      output=input
    CLASS DEFAULT
      output=IEEE_VALUE(output, IEEE_SIGNALING_NAN)
    END SELECT
  END FUNCTION convert_to_real_r8

!-------------------------------------------------------------------------------
!> convert to a COMPLEX(r8) type, return NaN if conversion fails
!-------------------------------------------------------------------------------
  ELEMENTAL FUNCTION convert_to_cmplx_r8(input) RESULT(output)
    IMPLICIT NONE

    !> input variable
    CLASS(*), INTENT(IN) :: input

    COMPLEX(r8) :: output
    REAL(r8) :: nan

    SELECT TYPE (input)
    TYPE IS (INTEGER(i4))
      output=CMPLX(input,0_i4,r8)
    TYPE IS (INTEGER(i8))
      output=CMPLX(input,0_i4,r8)
    TYPE IS (REAL(r4))
      output=CMPLX(input,0._r8,r8)
    TYPE IS (REAL(r8))
      output=CMPLX(input,0._r8,r8)
    TYPE IS (COMPLEX(r4))
      output=CMPLX(REAL(input),AIMAG(input),r8)
    TYPE IS (COMPLEX(r8))
      output=input
    CLASS DEFAULT
      nan=IEEE_VALUE(nan, IEEE_SIGNALING_NAN)
      output=CMPLX(nan,nan,r8)
    END SELECT
  END FUNCTION convert_to_cmplx_r8

END MODULE convert_type
