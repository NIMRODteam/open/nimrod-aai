!-------------------------------------------------------------------------------
!! types for parallel seam communication
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* types for parallel seam communication
!-------------------------------------------------------------------------------
MODULE seam_comm_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

!-------------------------------------------------------------------------------
!> data stucture for message sending of SEAM DATA
!-------------------------------------------------------------------------------
  TYPE :: send_type
    !> processor id to send message to
    INTEGER(i4) :: proc
    !> number of datums to send, a "datum" = nqty values
    INTEGER(i4) :: count
    !> which of my local blocks (1:nbl) the datum comes from
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: bl_loc
    !> which seam vertex (1:nvert) the datum comes from
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: vertex
    !> which vertex image (1:nimage) the datum corresponds to
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: image
    !> all values packed into one message, is a 1-d vector so that data can be
    !  packed contiguously regardless of what nqty is on a particular seaming
    !  call
    REAL(r8), DIMENSION(:), ALLOCATABLE :: data
    !> a complex version of data
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: cdata
  END TYPE send_type

!-------------------------------------------------------------------------------
!> data stucture for message receiving of SEAM DATA
!-------------------------------------------------------------------------------
  TYPE :: recv_type
    !> processor id of who will send me the message
    INTEGER(i4) :: proc
    !> number of datums I will receive, a "datum" = nqty values
    INTEGER(i4) :: count
    !> which of my local blocks (1:nbl) the datum gets summed to
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: bl_loc
    !> which seam vertex in my block the datum corresponds to
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: vertex
    !> unique ordering of this image in all representations so sums produce
    !  identical round-off
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: order
    !> all values packed into one message, received data will be packed
    !  contiguously regardless of what nqty is on a particular seaming call
    REAL(r8), DIMENSION(:), ALLOCATABLE :: data
    !> a complex version of data
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: cdata
  END TYPE recv_type

!-------------------------------------------------------------------------------
!> data stucture for performing SEAMING of values between blocks I own
!-------------------------------------------------------------------------------
  TYPE :: self_type
    !> which of my local blocks (1:nbl) the datum gets summed to
    INTEGER(i4) block_out
    !> which seam vertex (1:nvert) the datum gets summed to
    INTEGER(i4) vertex_out
    !> which of my local blocks (1:nbl) the datum comes from
    INTEGER(i4) block_in
    !> which seam vertex (1:nvert) the datum comes from
    INTEGER(i4) vertex_in
    !> unique ordering of this image in all (except degenerate
    !  points) representations so sums produce identical round-off
    INTEGER(i4) order
  END TYPE self_type

!-------------------------------------------------------------------------------
!> data stucture for message sending of SEGMENT DATA
!-------------------------------------------------------------------------------
  TYPE :: sendseg_type
    !> processor id to send message to
    INTEGER(i4) :: proc
    !> number of datums to send, a "datum" = nqty x nqty values
    INTEGER(i4) :: count
    !> which of my local blocks (1:nbl) the datum comes from
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: bl_loc
    !> which segment (1:nvert) the datum comes from
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: segment
    !> all values packed into one message,
    !  is a 1-d vector so that data can be packed contiguously
    REAL(r8), DIMENSION(:), ALLOCATABLE :: data
    !> a complex version of data
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: cdata
  END TYPE sendseg_type

!-------------------------------------------------------------------------------
!> data stucture for message receiving of SEGMENT DATA
!-------------------------------------------------------------------------------
  TYPE :: recvseg_type
    !> processor id of who will send me the message
    INTEGER(i4) :: proc
    !> number of datums I will receive, a "datum" = nqty x nqty values
    INTEGER(i4) :: count
    !> which of my local blocks (1:nbl) the datum gets summed to
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: bl_loc
    !> which segment in my block the datum corresponds to
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: segment
    !> all values packed into one message,
    !  received data will be packed contiguously
    REAL(r8), DIMENSION(:), ALLOCATABLE :: data
    !> a complex version of data
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: cdata
  END TYPE recvseg_type

!-------------------------------------------------------------------------------
!> data stucture for seaming SEGMENTS of values between blocks I own
!-------------------------------------------------------------------------------
  TYPE :: selfseg_type
    !> which of my local blocks (1:nbl) the datum gets summed to
    INTEGER(i4) block_out
    !> which segment (1:nvert) the datum gets summed to
    INTEGER(i4) segment_out
    !> which of my local blocks (1:nbl) the datum comes from
    INTEGER(i4) block_in
    !> which segment (1:nvert) the datum comes from
    INTEGER(i4) segment_in
  END TYPE selfseg_type

!-------------------------------------------------------------------------------
!> data stucture for seam communication
!-------------------------------------------------------------------------------
  TYPE :: seam_comm_type
    ! VERTEX communication data
    !> # of messages I will send
    INTEGER(i4) :: nsend=0_i4
    !> send(1:nsend) of send_type, one for each proc a message will be sent to
    TYPE(send_type), DIMENSION(:), ALLOCATABLE :: send
    !> # of messages I will receive
    INTEGER(i4) :: nrecv=0_i4
    !> recv(1:nrecv) one for each proc a message will come from
    TYPE(recv_type), DIMENSION(:), ALLOCATABLE :: recv
    !> array of requests for posting asynchronous receives
    TYPE(mpi_request), DIMENSION(:), ALLOCATABLE :: recv_request
    !> # of seam points whose images are also owned by me
    INTEGER(i4) :: nself=0_i4
    !> self(1:nself), one for each point, note that a pair of
    !  self-referencing pts will be stored twice
    TYPE(self_type), DIMENSION(:), ALLOCATABLE :: self
    ! SEGMENT communication data
    !> # of messages I will send
    INTEGER(i4) :: nsendseg=0_i4
    !> sendseg(1:nsendseg) one for each proc a message will be sent to
    TYPE(sendseg_type), DIMENSION(:), ALLOCATABLE :: sendseg
    !> # of messages I will receive
    INTEGER(i4) :: nrecvseg=0_i4
    !> recvseg(1:nrecvseg) one for each proc a message will come from
    TYPE(recvseg_type), DIMENSION(:), ALLOCATABLE :: recvseg
    !> array of requests for posting asynchronous receives
    TYPE(mpi_request), DIMENSION(:), ALLOCATABLE :: recvseg_request
    !> # of segments whose images are also owned by me
    INTEGER(i4) :: nselfseg=0_i4
    !> selfseg(1:nselfseg) one for each segment, note that a
    !  pair of self-referencing segments will be stored twice
    TYPE(selfseg_type), DIMENSION(:), ALLOCATABLE :: selfseg
  END TYPE seam_comm_type

END MODULE seam_comm_mod
