Code generation utilities.

Run `updateNamelists.py` to generate broadcast, h5 I/O and namelist read code.
The expectation is that the namelist is defined at the top of the file, either
within the module or within a type. The statement
```
!: namelist <name>
```
defines the start of namelist `<name>`. Multiple namelists may be defined.
The statement
```
!: computed
```
ends the namelist declaration block and allows for additional variables to be
defined that are not included in the namelist but are subsequently computed.
