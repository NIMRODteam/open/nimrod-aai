#!/usr/bin/env python3
import optparse
import os


def processFile(fileName):
    ''' adjust the continuation characters in a file '''
    infile = open(fileName, "r")
    outfile = open(fileName+'new.f', "w")
    for line in infile:
        newline = line
        if len(line) > 2:
            # Line length of 82 includes the new line character
            if line[-2] == "&" and len(line) != 82:
                newline = line[0:-2]
                while len(newline) < 80:
                    newline += " "
                newline += "&" + line[-1]
        outfile.write(newline)
    os.replace(fileName+'new.f', fileName)


def main():
    parser = optparse.OptionParser(usage='%prog [options] file.f')
    options, args = parser.parse_args()
    fileNames = []
    if len(args) == 1:
        fileNames.append(args[0])
    else:
        for root, dirs, files in os.walk("./", topdown=True):
            dirs[:] = [d for d in dirs
                       if not d.startswith('build')
                       and not d.startswith('public')]
            for file in files:
                if file.endswith(".f"):
                    fileNames.append(os.path.join(root, file))

    for fileName in fileNames:
        processFile(fileName)
    return


if __name__ == "__main__":
    main()
