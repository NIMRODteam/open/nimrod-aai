#!/usr/bin/env python3
import optparse
import os
import re
import sys


def processFile(fileName):
    '''
    Check that corresponding acc exit data is present for every acc enter data.
    This check expects that the corresponding acc exit data statement is placed
    in the same file.

    Also check that the async and if clauses are present on the proper
    statements.
    '''
    infile = open(fileName, "r")

    enterContLine = False
    exitContLine = False
    enterDataList = []
    exitDataList = []
    asyncifContLine = False
    foundAsync = False
    foundIf = False
    lineNumber = 0
    errCount = 0

    for line in infile:
        lineNumber += 1
        if 'acc enter data' in line or enterContLine:
            if line[-2] == "&":
                enterContLine = True
            else:
                enterContLine = False
            splitLine = line.split()
            for statement in splitLine:
                if statement.startswith('create') \
                        or statement.startswith('copyin'):
                    # assumes no space in statement, e.g. create(var1,var2)
                    enterDataList.extend(re.split(r'\(|,|\)', statement))
        if 'acc exit data' in line or exitContLine:
            if line[-2] == "&":
                exitContLine = True
            else:
                exitContLine = False
            splitLine = line.split()
            for statement in splitLine:
                if statement.startswith('delete'):
                    # assumes no space in statement, e.g. delete(var1,var2)
                    exitDataList.extend(re.split(r'\(|,|\)', statement))
        if any([substr in line
                for substr in ['acc parallel', 'acc kernel', 'acc update',
                               'acc enter data', 'acc exit data']]) \
                or asyncifContLine:
            if 'async(' in line:
                foundAsync = True
            if 'if(' in line:
                foundIf = True
            if line[-2] == "&":
                asyncifContLine = True
            else:
                asyncifContLine = False
                if not foundAsync:
                    print('In ' + fileName + ' line ' + str(lineNumber)
                          + ' missing async statement')
                    errCount += 1
                if not foundIf:
                    print('In ' + fileName + ' line ' + str(lineNumber)
                          + ' missing if statement')
                foundAsync = False
                foundIf = False

    enterDataList = list(filter(lambda a: a != 'create' and a != 'copyin'
                                and a != '', enterDataList))
    exitDataList = list(filter(lambda a: a != 'delete' and a != '',
                               exitDataList))

    for var in enterDataList:
        if var not in exitDataList:
            print('In ' + fileName + ' variable ' + var
                  + ' missing exit data statement')
            errCount += 1

    return errCount


def main():
    parser = optparse.OptionParser(usage='%prog [options] file.f')
    options, args = parser.parse_args()
    fileNames = []
    if len(args) == 1:
        fileNames.append(args[0])
    else:
        for root, dirs, files in os.walk("./", topdown=True):
            dirs[:] = [d for d in dirs
                       if not d.startswith('build')
                       if not d.startswith('nimlib')
                       and not d.startswith('public')]
            for file in files:
                if file.endswith(".f"):
                    fileNames.append(os.path.join(root, file))

    errCount = 0
    for fileName in fileNames:
        errCount += processFile(fileName)

    print('Found '+str(errCount)+' errors')
    return errCount


if __name__ == "__main__":
    sys.exit(main())
