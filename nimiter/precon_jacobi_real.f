!-------------------------------------------------------------------------------
!! Defines the Jacobi preconditioner
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> Defines the Jacobi preconditioner.
!
!  [Jacobi iteration](https://en.wikipedia.org/wiki/Jacobi_method)
!  for the problem \(A \mathbf{x} = \mathbf{b}\) is defined as
!  \begin{equation}
!    \mathbf{x}^{(k+1)} =
!      D^{-1} \cdot \left( \mathbf{b} - (L+U)\cdot \mathbf{x}^k \right)
!  \end{equation}
!  where \(\mathbf{x}^k\) is the solution vector at superscript iteration
!  number, \(k\), \(\mathbf{b}\) the RHS vector, and the matrix
!  \(A = L + U + D\) is decomposed in lower triangular, upper triangular and
!  diagonal components, respectively.
!
!  Rearranging terms, we find
!  \begin{equation}
!    \mathbf{x}^{(k+1)} = D^{-1} \cdot \mathbf{r}^k + \mathbf{x}^k
!  \end{equation}
!  where the residual vector is defined as
!  \(\mathbf{r}^k = \mathbf{b} - A \cdot \mathbf{x}^k\).
!
!  This form is related to the Jacobi
!  [preconditioner](https://en.wikipedia.org/wiki/Preconditioner),
!  \(D^{-1} \cdot \mathbf{r}^k\), used in `apply_precon`. The `update` step
!  computes the updated \(D^{-1}\) and stores the result as a vector.
!-------------------------------------------------------------------------------
MODULE precon_jacobi_real_mod
  USE local
  USE matrix_mod
  USE preconditioner_mod
  USE timer_mod
  USE vector_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: precon_jacobi_real

  CHARACTER(*), PARAMETER :: mod_name='precon_jacobi_real_mod'
!-------------------------------------------------------------------------------
!> Type that defines a Jacobi preconditioner for real data
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(precon_real) :: precon_jacobi_real
    !> store the inverse of the diagonal as a vector
    TYPE(rvec_storage), ALLOCATABLE, DIMENSION(:) :: inv_diag_mat
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(precon) :: alloc => alloc_real
    ! Abstract class deferred functions
    PROCEDURE, PASS(precon) :: dealloc => dealloc_real
    PROCEDURE, PASS(precon) :: update => update_real
    PROCEDURE, PASS(precon) :: apply_precon => apply_precon_real
  END TYPE precon_jacobi_real

CONTAINS

!-------------------------------------------------------------------------------
!* Allocate the preconditioner
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real(precon,vec)
    USE pardata_mod
    IMPLICIT NONE

    !> preconditioner to alloc
    CLASS(precon_jacobi_real), INTENT(INOUT) :: precon
    !> vector used determine types/sizes with mold
    CLASS(rvec_storage), DIMENSION(:), INTENT(IN) :: vec

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
    NULLIFY(precon%f_precon_init)
    NULLIFY(precon%f_precon_finalize)
    ALLOCATE(precon%inv_diag_mat(par%nbl))
    DO ibl=1,par%nbl
      CALL vec(ibl)%v%alloc_with_mold(precon%inv_diag_mat(ibl)%v)
      CALL precon%inv_diag_mat(ibl)%v%zero
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real

!-------------------------------------------------------------------------------
!* Deallocate the preconditioner
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real(precon)
    USE pardata_mod
    IMPLICIT NONE

    !> preconditioner to dealloc
    CLASS(precon_jacobi_real), INTENT(INOUT) :: precon

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real',iftn,idepth)
    DO ibl=1,par%nbl
      CALL precon%inv_diag_mat(ibl)%v%dealloc
    ENDDO
    DEALLOCATE(precon%inv_diag_mat)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!* Update the preconditioner after the matrix update
!-------------------------------------------------------------------------------
  SUBROUTINE update_real(precon,mat,seam)
    USE linalg_utils_mod
    USE pardata_mod
    USE seam_mod
    IMPLICIT NONE

    !> preconditioner to update
    CLASS(precon_jacobi_real), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'update_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   matrices are stored without seaming, first extract the diagonal
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL mat(ibl)%m%get_diag_as_vec(precon%inv_diag_mat(ibl)%v)
      CALL precon%inv_diag_mat(ibl)%v%edge_load_arr(seam%s(ibl),                &
                                                    do_avg=apply_ave_factor)
    ENDDO
!-------------------------------------------------------------------------------
!   next apply seaming
!-------------------------------------------------------------------------------
    CALL seam%edge_network
!-------------------------------------------------------------------------------
!   finally take the inverse that includes combined values along block borders
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(mat)
      CALL precon%inv_diag_mat(ibl)%v%edge_unload_arr(seam%s(ibl))
      CALL precon%inv_diag_mat(ibl)%v%invert
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE update_real

!-------------------------------------------------------------------------------
!* Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
  SUBROUTINE apply_precon_real(precon,mat,res,zee,adr)
    USE pardata_mod
    IMPLICIT NONE

    !> preconditioner to apply
    CLASS(precon_jacobi_real), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> the residual res = rhs - A x_k
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: res
    !> the preconditioned residual, zee = M^-1 res
    TYPE(rvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
    !> the full matrix vector product, A x_k
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: adr

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_precon_real',iftn,idepth)
    DO ibl=1,par%nbl
      CALL precon%inv_diag_mat(ibl)%v%apply_diag_matvec(res(ibl)%v,zee(ibl)%v)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_precon_real

END MODULE precon_jacobi_real_mod
