!-------------------------------------------------------------------------------
!! Defines the abstract interface for linear algebra matrices
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the abstract interface for linear algebra matrices.
!
!  Operations performed are matrix-vector multiplication, operations
!  to find the inverse using a
!  [Schur complement method](https://en.wikipedia.org/wiki/Schur_complement),
!  and enforcement of boundary and regularity conditions. Assignment operations
!  are provided for testing.
!
!  One of the major operations performed here is static condensation which
!  aids the computation by reducing the number of unknowns in direct matrix
!  solves. Here is a quick overview of this operation and how it related to
!  the API. Consider a matrix in block diagonal form,
!  \begin{equation}
!    A = \left[ \begin{matrix} A_{oo} & A_{oi}
!                           \\ A_{io} & A_{ii} \end{matrix} \right] \;.
!  \end{equation}
!  Here the \( o \) designates degrees of freedom associated with the boundary
!  of each element and \( i \) designates degrees of freedom associated with
!  the interior of each element. If \( A_{ii} \) is invertible then we can
!  find the inverse of \( A \) by finding the inverse of \( A_{ii} \) and
!  the inverse of the Schur complement,
!  \( S = A_{oo} - A_{oi} A_{ii}^{-1} A_{io} \) where
!  \begin{equation}
!    A^{-1}= \left[ \begin{matrix} I_{oo} & 0
!                           \\ -A_{ii}^{-1} A_{io} & I_{ii} \end{matrix} \right]
!            \left[ \begin{matrix} S^{-1} & 0
!                               \\ 0 & A_{ii}^{-1} \end{matrix} \right]
!            \left[ \begin{matrix} I_{oo} & -A_{oi} A_{ii}^{-1}
!                               \\ 0 & I_{ii} \end{matrix} \right]
!    \label{eq:schur}
!  \end{equation}
!  and the \( I_{oo} \) and \( I_{ii} \) are identity matrices of the
!  appropriate size. Thus finding the inverse of \( A \) only requires matrix
!  multiplication and finding the inverse of the smaller matrices \( A_{ii} \)
!  and \( S \). As the interior nodes only couple to degrees of freedom inside
!  each element finding \( A_{ii}^{-1} \) can be broken into element-wise
!  computations.
!
!  With respect to the API, the operation `elim_inv_int` transforms the matrix
!  storage to the following form,
!  \begin{equation}
!    A_{elim} = \left[ \begin{matrix} S & A_{oi}
!                               \\ A_{io} & A_{ii}^{-1} \end{matrix} \right] \;.
!  \end{equation}
!  The operation `elim_presolve` is performed on the RHS vector of the system
!  \( A x = b \) and applies the right most matrix multiplication and the middle
!  matrix multiplication associated with the lower right hand block matrix in
!  Eqn.~\eqref{eq:schur}. Thus given a vector
!  \begin{equation}
!    b = \left[ \begin{matrix} b_o \\ b_i \end{matrix} \right] \;,
!  \end{equation}
!  the resulting vector after application of `elim_presolve` is
!  \begin{equation}
!    b_{elim} = \left[ \begin{matrix} b_o - A_{oi} A_{ii}^{-1} b_i
!                                  \\ A_{ii}^{-1} b_i \end{matrix} \right] \;.
!  \end{equation}
!  For systems with interior elimination NIMROD's solvers only operate on the
!  Schur complement. Thus they solve
!  \begin{equation}
!    S x_o = b_o - A_{oi} A_{ii}^{-1} b_i
!  \end{equation}
!  for \( x_o \). Finally, the operation `elim_postsolve` is applied which
!  computes the final matrix multiplication by applying the left most matrix
!  multiplication in Eqn.~\eqref{eq:schur}. The resulting vector is the
!  full solution
!  \begin{equation}
!    x = \left[ \begin{matrix} x_o
!                           \\ A_{ii}^{-1} b_i - A_{ii}^{-1} A_{io} x_o
!                           \end{matrix} \right] \;.
!  \end{equation}
!
!  Naming conventions for derived types should use the format
!  mat\_{shape}\_{dim}\_{type} where, for example, shape is rect or tri, dim
!  is 1D or 2D, and type is real or comp.
!-------------------------------------------------------------------------------
MODULE matrix_mod
  USE compressed_conversion_mod
  USE local
  USE vector_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: rmat_storage,cmat_storage

  PUBLIC :: rmatrix,cmatrix

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  matrix operations with a real data type
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: rmatrix
    !> Number of quantities (e.g. 3 for vector)
    INTEGER(i4) :: nqty=0
    !> Unit number of degrees of freedom (ndof per element for a scalar)
    INTEGER(i4) :: u_ndof=0
    !> Number of elements in field
    INTEGER(i4) :: nel=0
    !> dimensionality of field (e.g. 1D, 2D, etc.)
    INTEGER(i4) :: ndim=0
    !> character(s) describing vector components / scalar
    !  e.g. 's' for scalar or 'rzp' for a vector
    CHARACTER(1), DIMENSION(:), ALLOCATABLE :: vcomp
    !> true if interior values have been eliminated
    LOGICAL :: eliminated=.FALSE.
    !> true if symmetric
    LOGICAL :: symmetric=.FALSE.
    !> applied essential condition
    CHARACTER(16) :: essential_cond="none"
    !> associated Fourier component
    INTEGER(i4) :: fcomp=0
    !> diagonal scale factor
    REAL(r8) :: diag_scale
    !> map of block local data to a global DOF index
    CLASS(ivector), ALLOCATABLE :: dof_map
    !> count of matrix global number of nonzeros per row
    CLASS(ivector), ALLOCATABLE :: nnz_map
    !> data for conversion to a compression storage format
    TYPE(compressed_conversion_type), ALLOCATABLE :: mat_conv
    !> sent matrix values by MPI processor (size nsend)
    TYPE(comm_vals_real), ALLOCATABLE :: send_vals(:)
    !> srecieved matrix values by MPIprocessor (size nrecv)
    TYPE(comm_vals_real), ALLOCATABLE :: recv_vals(:)
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    PROCEDURE(dealloc_real), PASS(mat), DEFERRED :: dealloc
    PROCEDURE(matvec_real), PASS(mat), DEFERRED :: matvec
    PROCEDURE(assemble_real), PASS(mat), DEFERRED :: assemble
    PROCEDURE(zero_real), PASS(mat), DEFERRED :: zero
    PROCEDURE(elim_inv_int_real), PASS(mat), DEFERRED :: elim_inv_int
    PROCEDURE(elim_presolve_real), PASS(mat), DEFERRED :: elim_presolve
    PROCEDURE(elim_postsolve_real), PASS(mat), DEFERRED :: elim_postsolve
    PROCEDURE(find_diag_scale_real), PASS(mat), DEFERRED :: find_diag_scale
    PROCEDURE(dirichlet_bc_real), PASS(mat), DEFERRED :: dirichlet_bc
    PROCEDURE(regularity_real), PASS(mat), DEFERRED :: regularity
    PROCEDURE(get_diag_as_vec_real), PASS(mat), DEFERRED :: get_diag_as_vec
    PROCEDURE(set_identity_real), PASS(mat), DEFERRED :: set_identity
    PROCEDURE(init_dof_map_real), PASS(mat), DEFERRED :: init_dof_map
    PROCEDURE(init_nnz_map_real), PASS(mat), DEFERRED :: init_nnz_map
    PROCEDURE(init_local_sparsity_real), PASS(mat), DEFERRED ::                 &
                                                             init_local_sparsity
    PROCEDURE(send_comm_sparsity_real), PASS(mat), DEFERRED ::                  &
                                                             send_comm_sparsity
    PROCEDURE(set_comm_sparsity_real), PASS(mat), DEFERRED :: set_comm_sparsity
    PROCEDURE(init_matrix_to_compressed_comm_real), PASS(mat), DEFERRED ::      &
                                                  init_matrix_to_compressed_comm
    PROCEDURE(send_matrix_to_compressed_comm_real), PASS(mat), DEFERRED ::      &
                                                  send_matrix_to_compressed_comm
    PROCEDURE(finalize_matrix_to_compressed_comm_real), PASS(mat), DEFERRED ::  &
                                              finalize_matrix_to_compressed_comm
    PROCEDURE(export_matrix_to_compressed_real), PASS(mat), DEFERRED ::         &
                                                     export_matrix_to_compressed
  END TYPE rmatrix

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  matrix operations with a complex data type
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: cmatrix
    !> Number of quantities (e.g. 3 for vector)
    INTEGER(i4) :: nqty=0
    !> Unit number of degrees of freedom (ndof per element for a scalar)
    INTEGER(i4) :: u_ndof=0
    !> Number of elements in field
    INTEGER(i4) :: nel=0
    !> dimensionality of field (e.g. 1D, 2D, etc.)
    INTEGER(i4) :: ndim=0
    !> character describing vector components / scalar
    CHARACTER(1), DIMENSION(:), ALLOCATABLE :: vcomp
    !> true if interior values have been eliminated
    LOGICAL :: eliminated=.FALSE.
    !> true if Hermitian
    LOGICAL :: hermitian=.FALSE.
    !> applied essential condition
    CHARACTER(16) :: essential_cond="none"
    !> associated Fourier component
    INTEGER(i4) :: fcomp=0
    !> diagonal scale factor
    REAL(r8) :: diag_scale
    !> map of block local data to a global DOF index
    CLASS(ivector), ALLOCATABLE :: dof_map
    !> count of matrix global number of nonzeros per row
    CLASS(ivector), ALLOCATABLE :: nnz_map
    !> data for conversion to a compression storage format
    TYPE(compressed_conversion_type), ALLOCATABLE :: mat_conv
    !> sent matrix values by MPI processor (size nsend)
    TYPE(comm_vals_comp), ALLOCATABLE :: send_vals(:)
    !> srecieved matrix values by MPIprocessor (size nrecv)
    TYPE(comm_vals_comp), ALLOCATABLE :: recv_vals(:)
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    PROCEDURE(dealloc_comp), PASS(mat), DEFERRED :: dealloc
    PROCEDURE(matvecim_comp), PASS(mat), DEFERRED :: matvecim
    PROCEDURE(matvec1m_comp), PASS(mat), DEFERRED :: matvec1m
    PROCEDURE(assemble_comp), PASS(mat), DEFERRED :: assemble
    PROCEDURE(zero_comp), PASS(mat), DEFERRED :: zero
    PROCEDURE(elim_inv_int_comp), PASS(mat), DEFERRED :: elim_inv_int
    PROCEDURE(elim_presolveim_comp), PASS(mat), DEFERRED :: elim_presolveim
    PROCEDURE(elim_presolve1m_comp), PASS(mat), DEFERRED :: elim_presolve1m
    PROCEDURE(elim_postsolveim_comp), PASS(mat), DEFERRED :: elim_postsolveim
    PROCEDURE(elim_postsolve1m_comp), PASS(mat), DEFERRED :: elim_postsolve1m
    PROCEDURE(find_diag_scale_comp), PASS(mat), DEFERRED :: find_diag_scale
    PROCEDURE(dirichlet_bc_comp), PASS(mat), DEFERRED :: dirichlet_bc
    PROCEDURE(regularity_comp), PASS(mat), DEFERRED :: regularity
    PROCEDURE(get_diag_as_vec_comp), PASS(mat), DEFERRED :: get_diag_as_vecim
    PROCEDURE(get_diag_as_vec_cv1m), PASS(mat), DEFERRED :: get_diag_as_vec1m
    PROCEDURE(set_identity_comp), PASS(mat), DEFERRED :: set_identity
    !TODO: complex export
    GENERIC :: matvec => matvecim,matvec1m
    GENERIC :: elim_presolve => elim_presolveim,elim_presolve1m
    GENERIC :: elim_postsolve => elim_postsolveim,elim_postsolve1m
    GENERIC :: get_diag_as_vec => get_diag_as_vecim,get_diag_as_vec1m
  END TYPE cmatrix

!-------------------------------------------------------------------------------
!* container for rmatrix
!-------------------------------------------------------------------------------
  TYPE :: rmat_storage
    CLASS(rmatrix), ALLOCATABLE :: m
  END TYPE rmat_storage

!-------------------------------------------------------------------------------
!* container for cmatrix
!-------------------------------------------------------------------------------
  TYPE :: cmat_storage
    CLASS(cmatrix), ALLOCATABLE :: m
  END TYPE cmat_storage

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  Deallocate the matrix
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_real(mat)
      IMPORT rmatrix
      !> matrix to dealloc
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!*  Matrix vector product
!-------------------------------------------------------------------------------
    SUBROUTINE matvec_real(mat,operand,output)
      USE local
      USE vector_mod
      IMPORT rmatrix
      !> matrix to multiply
      CLASS(rmatrix), INTENT(IN) :: mat
      !> vector to multiply
      CLASS(rvector), INTENT(IN) :: operand
      !> result
      CLASS(rvector), INTENT(INOUT) :: output
    END SUBROUTINE matvec_real

!-------------------------------------------------------------------------------
!>  transfer contributions from integrand into internal storage as a plus
!   equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_real(mat,integrand)
      USE local
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)
    END SUBROUTINE assemble_real

!-------------------------------------------------------------------------------
!*  set matrix to zero
!-------------------------------------------------------------------------------
    SUBROUTINE zero_real(mat)
      IMPORT rmatrix
      !> matrix to set to zero
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE zero_real

!-------------------------------------------------------------------------------
!*  Eliminate interior data from the matrix
!-------------------------------------------------------------------------------
    SUBROUTINE elim_inv_int_real(mat)
      USE vector_mod
      IMPORT rmatrix
      !> matrix to eliminate
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE elim_inv_int_real

!-------------------------------------------------------------------------------
!*  Eliminate interior vector data
!-------------------------------------------------------------------------------
    SUBROUTINE elim_presolve_real(mat,input,output)
      USE vector_mod
      IMPORT rmatrix
      !> matrix to use for elimination
      CLASS(rmatrix), INTENT(IN) :: mat
      !> vector on which to eliminate interior data
      CLASS(rvector), INTENT(IN) :: input
      !> vector on which to store eliminated form
      CLASS(rvector), INTENT(INOUT) :: output
    END SUBROUTINE elim_presolve_real

!-------------------------------------------------------------------------------
!*  Restore interior vector data
!-------------------------------------------------------------------------------
    SUBROUTINE elim_postsolve_real(mat,input,output)
      USE vector_mod
      IMPORT rmatrix
      !> matrix to use for elimination
      CLASS(rmatrix), INTENT(IN) :: mat
      !> vector from which to restore interior data
      CLASS(rvector), INTENT(INOUT) :: input
      !> vector to which to restore full data
      CLASS(rvector), INTENT(INOUT) :: output
    END SUBROUTINE elim_postsolve_real

!-------------------------------------------------------------------------------
!*  Find the diagonal scaling factor
!-------------------------------------------------------------------------------
    SUBROUTINE find_diag_scale_real(mat)
      USE local
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE find_diag_scale_real

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
    SUBROUTINE dirichlet_bc_real(mat,component,edge,symm)
      USE local
      USE edge_mod
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> flag to determine normal/tangential/scalar behavior
      CHARACTER(*), INTENT(IN) :: component
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
      !> flag for symmetric boundary
      CHARACTER(*), INTENT(IN), OPTIONAL :: symm
    END SUBROUTINE dirichlet_bc_real

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
    SUBROUTINE regularity_real(mat,edge)
      USE local
      USE edge_mod
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE regularity_real

!-------------------------------------------------------------------------------
!*  Extract the diagonal as a vector
!-------------------------------------------------------------------------------
    SUBROUTINE get_diag_as_vec_real(mat,output_vec)
      USE local
      USE vector_mod
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(IN) :: mat
      !> output vector
      CLASS(rvector), INTENT(INOUT) :: output_vec
    END SUBROUTINE get_diag_as_vec_real

!-------------------------------------------------------------------------------
!*  Set matrix to the identity tensor for testing
!-------------------------------------------------------------------------------
    SUBROUTINE set_identity_real(mat,edge)
      USE edge_mod
      USE local
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> associated edge
      TYPE(edge_type), OPTIONAL, INTENT(IN) :: edge
    END SUBROUTINE set_identity_real

!-------------------------------------------------------------------------------
!*  Allocate and initialize an integer DOF map
!-------------------------------------------------------------------------------
    SUBROUTINE init_dof_map_real(mat,edge,skip_elim_interior,ndof)
      USE edge_mod
      USE local
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> associated edge
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
      LOGICAL, INTENT(IN) :: skip_elim_interior
      !> number of DOFs owned by this matrix block
      INTEGER(i4), INTENT(OUT) :: ndof
    END SUBROUTINE init_dof_map_real

!-------------------------------------------------------------------------------
!*  Finalize the initialization of the integer nnz map
!-------------------------------------------------------------------------------
    SUBROUTINE init_nnz_map_real(mat,edge,skip_elim_interior,nnz)
      USE edge_mod
      USE local
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> associated edge
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
      LOGICAL, INTENT(IN) :: skip_elim_interior
      !> number of nonzero matrix elements owned by this matrix block
      INTEGER(i4), INTENT(OUT) :: nnz
    END SUBROUTINE init_nnz_map_real

!-------------------------------------------------------------------------------
!*  Set local sparsity pattern
!-------------------------------------------------------------------------------
    SUBROUTINE init_local_sparsity_real(mat,csm,edge,skip_elim_interior,        &
                                        start_rows)
      USE compressed_matrix_real_mod
      USE edge_mod
      USE local
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> compressed sparse matrix
      TYPE(compressed_matrix_real), INTENT(INOUT) :: csm
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
      !> true if interior DOFs are (to be) eliminated and no longer valid DOFs
      LOGICAL, INTENT(IN) :: skip_elim_interior
      !> starting rows in DOF map for each global block
      INTEGER(i4), INTENT(IN) :: start_rows(:)
    END SUBROUTINE init_local_sparsity_real

!-------------------------------------------------------------------------------
!*  Send communicated sparsity pattern and associated data
!-------------------------------------------------------------------------------
    SUBROUTINE send_comm_sparsity_real(mat)
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE send_comm_sparsity_real

!-------------------------------------------------------------------------------
!*  Set communicated sparsity pattern and associated data
!-------------------------------------------------------------------------------
    SUBROUTINE set_comm_sparsity_real(mat,csm)
      USE compressed_matrix_real_mod
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> compressed sparse matrix
      TYPE(compressed_matrix_real), INTENT(INOUT) :: csm
    END SUBROUTINE set_comm_sparsity_real

!-------------------------------------------------------------------------------
!>  Start export of matrix values to a compressed format by posting recv
!   communication and loading values
!-------------------------------------------------------------------------------
    SUBROUTINE init_matrix_to_compressed_comm_real(mat)
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE init_matrix_to_compressed_comm_real

!-------------------------------------------------------------------------------
!>  Post sends of matrix data. This is a separate function from init as it
!   requires GPU synchornization before sends
!-------------------------------------------------------------------------------
    SUBROUTINE send_matrix_to_compressed_comm_real(mat)
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE send_matrix_to_compressed_comm_real

!-------------------------------------------------------------------------------
!*  Read received values into the compressed format
!-------------------------------------------------------------------------------
    SUBROUTINE finalize_matrix_to_compressed_comm_real(mat,csm)
      USE compressed_matrix_real_mod
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> compressed sparse matrix
      TYPE(compressed_matrix_real), INTENT(INOUT) :: csm
    END SUBROUTINE finalize_matrix_to_compressed_comm_real

!-------------------------------------------------------------------------------
!*  Export matrix values to a compressed format
!-------------------------------------------------------------------------------
    SUBROUTINE export_matrix_to_compressed_real(mat,csm)
      USE compressed_matrix_real_mod
      IMPORT rmatrix
      !> matrix
      CLASS(rmatrix), INTENT(INOUT) :: mat
      !> compressed sparse matrix
      TYPE(compressed_matrix_real), INTENT(INOUT) :: csm
    END SUBROUTINE export_matrix_to_compressed_real

!-------------------------------------------------------------------------------
!*  Deallocate the matrix
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_comp(mat)
      IMPORT cmatrix
      !> matrix to dealloc
      CLASS(cmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE dealloc_comp

!-------------------------------------------------------------------------------
!*  Matrix vector product
!-------------------------------------------------------------------------------
    SUBROUTINE matvecim_comp(mat,operand,output,ifop,ifout)
      USE local
      USE vector_mod
      IMPORT cmatrix
      !> matrix to multiply
      CLASS(cmatrix), INTENT(IN) :: mat
      !> vector to multiply
      CLASS(cvector), INTENT(IN) :: operand
      !> result
      CLASS(cvector), INTENT(INOUT) :: output
      !> Fourier index of operand
      INTEGER(i4), INTENT(IN) :: ifop
      !> Fourier index of output
      INTEGER(i4), INTENT(IN) :: ifout
    END SUBROUTINE matvecim_comp

!-------------------------------------------------------------------------------
!*  Matrix vector product
!-------------------------------------------------------------------------------
    SUBROUTINE matvec1m_comp(mat,operand,output)
      USE local
      USE vector_mod
      IMPORT cmatrix
      !> matrix to multiply
      CLASS(cmatrix), INTENT(IN) :: mat
      !> vector to multiply
      CLASS(cvector1m), INTENT(IN) :: operand
      !> result
      CLASS(cvector1m), INTENT(INOUT) :: output
    END SUBROUTINE matvec1m_comp

!-------------------------------------------------------------------------------
!>  transfer contributions from integrand into internal storage as a plus
!   equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_comp(mat,integrand)
      USE local
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(INOUT) :: mat
      !> integrand array
      COMPLEX(r8), INTENT(IN) :: integrand(:,:,:,:,:)
    END SUBROUTINE assemble_comp

!-------------------------------------------------------------------------------
!*  set matrix to zero
!-------------------------------------------------------------------------------
    SUBROUTINE zero_comp(mat)
      IMPORT cmatrix
      !> matrix to set to zero
      CLASS(cmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE zero_comp

!-------------------------------------------------------------------------------
!*  Eliminate interior data from the matrix
!-------------------------------------------------------------------------------
    SUBROUTINE elim_inv_int_comp(mat)
      IMPORT cmatrix
      !> matrix to eliminate
      CLASS(cmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE elim_inv_int_comp

!-------------------------------------------------------------------------------
!*  Eliminate interior vector data
!-------------------------------------------------------------------------------
    SUBROUTINE elim_presolveim_comp(mat,input,output,imode)
      USE local
      USE vector_mod
      IMPORT cmatrix
      !> matrix to use for elimination
      CLASS(cmatrix), INTENT(IN) :: mat
      !> vector on which to eliminate interior data
      CLASS(cvector), INTENT(IN) :: input
      !> vector on which to store eliminated form
      CLASS(cvector), INTENT(INOUT) :: output
      !> mode to eliminate in multi-mode complex input/output
      INTEGER(i4), INTENT(IN) :: imode
    END SUBROUTINE elim_presolveim_comp

!-------------------------------------------------------------------------------
!*  Eliminate interior vector data
!-------------------------------------------------------------------------------
    SUBROUTINE elim_presolve1m_comp(mat,input,output)
      USE vector_mod
      IMPORT cmatrix
      !> matrix to use for elimination
      CLASS(cmatrix), INTENT(IN) :: mat
      !> vector on which to eliminate interior data
      CLASS(cvector1m), INTENT(IN) :: input
      !> vector on which to store eliminated form
      CLASS(cvector1m), INTENT(INOUT) :: output
    END SUBROUTINE elim_presolve1m_comp

!-------------------------------------------------------------------------------
!*  Restore interior vector data
!-------------------------------------------------------------------------------
    SUBROUTINE elim_postsolveim_comp(mat,input,output,imode)
      USE local
      USE vector_mod
      IMPORT cmatrix
      !> matrix to use for elimination
      CLASS(cmatrix), INTENT(IN) :: mat
      !> vector from which to restore interior data
      CLASS(cvector), INTENT(INOUT) :: input
      !> vector to which to restore full data
      CLASS(cvector), INTENT(INOUT) :: output
      !> mode to restore in multi-mode complex input/output
      INTEGER(i4), INTENT(IN) :: imode
    END SUBROUTINE elim_postsolveim_comp

!-------------------------------------------------------------------------------
!*  Restore interior vector data
!-------------------------------------------------------------------------------
    SUBROUTINE elim_postsolve1m_comp(mat,input,output)
      USE vector_mod
      IMPORT cmatrix
      !> matrix to use for elimination
      CLASS(cmatrix), INTENT(IN) :: mat
      !> vector from which to restore interior data
      CLASS(cvector1m), INTENT(INOUT) :: input
      !> vector to which to restore full data
      CLASS(cvector1m), INTENT(INOUT) :: output
    END SUBROUTINE elim_postsolve1m_comp

!-------------------------------------------------------------------------------
!*  Find the diagonal scaling factor
!-------------------------------------------------------------------------------
    SUBROUTINE find_diag_scale_comp(mat)
      USE local
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE find_diag_scale_comp

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
    SUBROUTINE dirichlet_bc_comp(mat,component,edge,symm)
      USE local
      USE edge_mod
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(INOUT) :: mat
      !> flag to determine normal/tangential/scalar behavior
      CHARACTER(*), INTENT(IN) :: component
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
      !> flag for symmetric boundary
      CHARACTER(*), INTENT(IN), OPTIONAL :: symm
    END SUBROUTINE dirichlet_bc_comp

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
    SUBROUTINE regularity_comp(mat,edge)
      USE local
      USE edge_mod
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(INOUT) :: mat
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE regularity_comp

!-------------------------------------------------------------------------------
!*  Extract the diagonal as a vector
!-------------------------------------------------------------------------------
    SUBROUTINE get_diag_as_vec_comp(mat,output_vec,imode)
      USE local
      USE vector_mod
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(IN) :: mat
      !> output vector
      CLASS(cvector), INTENT(INOUT) :: output_vec
      !> mode to fill in multi-mode complex output_vec
      INTEGER(i4), INTENT(IN) :: imode
    END SUBROUTINE get_diag_as_vec_comp

!-------------------------------------------------------------------------------
!*  Extract the diagonal as a vector
!-------------------------------------------------------------------------------
    SUBROUTINE get_diag_as_vec_cv1m(mat,output_vec)
      USE local
      USE vector_mod
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(IN) :: mat
      !> output vector
      CLASS(cvector1m), INTENT(INOUT) :: output_vec
    END SUBROUTINE get_diag_as_vec_cv1m

!-------------------------------------------------------------------------------
!*  Set matrix to the identity tensor for testing
!-------------------------------------------------------------------------------
    SUBROUTINE set_identity_comp(mat)
      USE local
      IMPORT cmatrix
      !> matrix
      CLASS(cmatrix), INTENT(INOUT) :: mat
    END SUBROUTINE set_identity_comp
  END INTERFACE

END MODULE matrix_mod
