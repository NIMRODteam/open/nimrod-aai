!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_int included in vec_rect_2D.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a int vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_int(ivec,poly_degree,mx,my,nqty,id)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_int), INTENT(INOUT) :: ivec
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_int',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    ivec%nqty=nqty
    ivec%mx=mx
    ivec%my=my
    ivec%n_side=poly_degree-1
    ivec%n_int=(poly_degree-1)**2
    ivec%u_ndof=(poly_degree+1)**2
    ivec%pd=poly_degree
    ivec%nel=mx*my
    ivec%ndim=2
    ivec%id=id
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(ivec%arr(nqty,0:mx,0:my))
      NULLIFY(ivec%arri,ivec%arrh,ivec%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(ivec%arr(nqty,0:mx,0:my))
      ALLOCATE(ivec%arrh(nqty,poly_degree-1,1:mx,0:my))
      ALLOCATE(ivec%arrv(nqty,poly_degree-1,0:mx,1:my))
      ALLOCATE(ivec%arri(nqty,(poly_degree-1)**2,1:mx,1:my))
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(ivec%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(ivec%arr)+SIZEOF(ivec%arrh)+SIZEOF(ivec%arrv)              &
             +SIZEOF(ivec%arri)+SIZEOF(ivec),i4)
      CALL memlogger%update(ivec%mem_id,'ivec'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_int

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_int(ivec)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_int), INTENT(INOUT) :: ivec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_int',iftn,idepth)
    IF (ASSOCIATED(ivec%arr)) THEN
      DEALLOCATE(ivec%arr)
      NULLIFY(ivec%arr)
    ENDIF
    IF (ASSOCIATED(ivec%arrh)) THEN
      DEALLOCATE(ivec%arrh)
      NULLIFY(ivec%arrh)
    ENDIF
    IF (ASSOCIATED(ivec%arrv)) THEN
      DEALLOCATE(ivec%arrv)
      NULLIFY(ivec%arrv)
    ENDIF
    IF (ASSOCIATED(ivec%arri)) THEN
      DEALLOCATE(ivec%arri)
      NULLIFY(ivec%arri)
    ENDIF
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(ivec%mem_id,'ivec'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_int

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_int(ivec,new_ivec,nqty,pd)
    IMPLICIT NONE

    !> reference vector to mold
    CLASS(vec_rect_2D_int), INTENT(IN) :: ivec
    !> new vector to be created
    CLASS(ivector), ALLOCATABLE, INTENT(OUT) :: new_ivec
    !> number of quantities, ivec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, ivec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_int',iftn,idepth)
    new_nqty=ivec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=ivec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the base allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_int::new_ivec)
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_ivec%skip_elim_interior=ivec%skip_elim_interior
!-------------------------------------------------------------------------------
!   call the allocation routine
!-------------------------------------------------------------------------------
    SELECT TYPE (new_ivec)
    TYPE IS (vec_rect_2D_int)
      CALL new_ivec%alloc(new_pd,ivec%mx,ivec%my,new_nqty,ivec%id)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_int

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_int(ivec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_int), INTENT(INOUT) :: ivec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_int',iftn,idepth)
    IF (ASSOCIATED(ivec%arr)) THEN
      ASSOCIATE (arr=>ivec%arr)
        arr=0_i4
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(ivec%arrh)) THEN
      ASSOCIATE (arrh=>ivec%arrh)
        arrh=0_i4
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(ivec%arrv)) THEN
      ASSOCIATE (arrv=>ivec%arrv)
        arrv=0_i4
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(ivec%arri)) THEN
      ASSOCIATE (arri=>ivec%arri)
        arri=0_i4
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_int

!-------------------------------------------------------------------------------
!> Add a constant if entry is greater than zero, otherwise set entry to zero.
!  This is done to determine the global DOF indexing and in preparation for
!  seaming. Negative values in the DOF map are self-periodic and set to zero.
!-------------------------------------------------------------------------------
  SUBROUTINE add_row_count_int(ivec,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_int), INTENT(INOUT) :: ivec
    !> constant to add
    INTEGER(i4), INTENT(IN) :: const

    INTEGER(i4) :: iy,ix,iq,is,ii
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'add_row_count_int',iftn,idepth)
    IF (ASSOCIATED(ivec%arr)) THEN
      ASSOCIATE (arr=>ivec%arr)
        DO iy=0,ivec%my
          DO ix=0,ivec%mx
            DO iq=1,ivec%nqty
              IF (arr(iq,ix,iy)>=0) THEN
                arr(iq,ix,iy)=arr(iq,ix,iy)+const
              ELSE
                arr(iq,ix,iy)=0
              ENDIF
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(ivec%arrh)) THEN
      ASSOCIATE (arrh=>ivec%arrh)
        DO iy=0,ivec%my
          DO ix=1,ivec%mx
            DO is=1,ivec%n_side
              DO iq=1,ivec%nqty
                IF (arrh(iq,is,ix,iy)>=0) THEN
                  arrh(iq,is,ix,iy)=arrh(iq,is,ix,iy)+const
                ELSE
                  arrh(iq,is,ix,iy)=0
                ENDIF
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(ivec%arrv)) THEN
      ASSOCIATE (arrv=>ivec%arrv)
        DO iy=1,ivec%my
          DO ix=0,ivec%mx
            DO is=1,ivec%n_side
              DO iq=1,ivec%nqty
                IF (arrv(iq,is,ix,iy)>=0) THEN
                  arrv(iq,is,ix,iy)=arrv(iq,is,ix,iy)+const
                ELSE
                  arrv(iq,is,ix,iy)=0
                ENDIF
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(ivec%arri)) THEN
      ASSOCIATE (arri=>ivec%arri)
        DO iy=1,ivec%my
          DO ix=1,ivec%mx
            DO ii=1,ivec%n_int
              DO iq=1,ivec%nqty
                IF (arri(iq,ii,ix,iy)>=0) THEN
                  arri(iq,ii,ix,iy)=arri(iq,ii,ix,iy)+const
                ELSE
                  arri(iq,ii,ix,iy)=0
                ENDIF
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_row_count_int

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_int(ivec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_int), INTENT(IN) :: ivec
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_int',iftn,idepth)
    DO iv=1,ivec%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=ivec%mx+1,ivec%mx+ivec%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=ivec%mx+ivec%my+1,2*ivec%mx+ivec%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*ivec%mx+ivec%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_int

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_int(ivec,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_int), INTENT(IN) :: ivec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_int',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=ivec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=ivec%n_side
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%nmodes_loaded=0_i4
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vert_in(1:nq,iv)=REAL(ivec%arr(1:nq,edge%vertex(iv)%intxy(1),        &
                                 edge%vertex(iv)%intxy(2)),r8)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  load side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            edge%seg_in(iq:nq+iq-1,iv)=REAL(ivec%arrh(1:nq,is,ix,iy),r8)
            iq=iq+nq
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            edge%seg_in(iq:nq+iq-1,iv)=REAL(ivec%arrv(1:nq,is,ix,iy),r8)
            iq=iq+nq
          ENDDO
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_int

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_int(ivec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_int), INTENT(INOUT) :: ivec
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_int',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      ivec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))=         &
        NINT(edge%vert_out(1:nq,iv),i4)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  unload side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            ivec%arrh(1:nq,is,ix,iy)=NINT(edge%seg_out(iq:nq+iq-1,iv),i4)
            iq=iq+nq
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            ivec%arrv(1:nq,is,ix,iy)=NINT(edge%seg_out(iq:nq+iq-1,iv),i4)
            iq=iq+nq
          ENDDO
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_int
