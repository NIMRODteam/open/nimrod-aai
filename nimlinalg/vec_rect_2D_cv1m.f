!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_cv1m included in vec_rect_2D.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a complex single-mode vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cv1m(cv1m,poly_degree,mx,my,nqty,id)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    cv1m%nqty=nqty
    cv1m%mx=mx
    cv1m%my=my
    cv1m%n_side=poly_degree-1
    cv1m%n_int=(poly_degree-1)**2
    cv1m%u_ndof=(poly_degree+1)**2
    cv1m%pd=poly_degree
    cv1m%nel=mx*my
    cv1m%ndim=2
    cv1m%id=id
    cv1m%inf_norm_size=4
    cv1m%l2_dot_size=7
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(cv1m%arr(nqty,0:mx,0:my))
      NULLIFY(cv1m%arri,cv1m%arrh,cv1m%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(cv1m%arr(nqty,0:mx,0:my))
      ALLOCATE(cv1m%arrh(nqty,poly_degree-1,1:mx,0:my))
      ALLOCATE(cv1m%arrv(nqty,poly_degree-1,0:mx,1:my))
      ALLOCATE(cv1m%arri(nqty,(poly_degree-1)**2,1:mx,1:my))
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(cv1m%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(cv1m%arr)+SIZEOF(cv1m%arrh)+SIZEOF(cv1m%arrv)              &
             +SIZEOF(cv1m%arri)+SIZEOF(cv1m),i4)
      CALL memlogger%update(cv1m%mem_id,'cv1m'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cv1m

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_cv1m(cv1m)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) THEN
      DEALLOCATE(cv1m%arr)
      NULLIFY(cv1m%arr)
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      DEALLOCATE(cv1m%arrh)
      NULLIFY(cv1m%arrh)
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      DEALLOCATE(cv1m%arrv)
      NULLIFY(cv1m%arrv)
    ENDIF
    IF (ASSOCIATED(cv1m%arri)) THEN
      DEALLOCATE(cv1m%arri)
      NULLIFY(cv1m%arri)
    ENDIF
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(cv1m%mem_id,'cv1m'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_cv1m

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_cv1m(cv1m,new_cv1m,nqty,pd)
    IMPLICIT NONE

    !> reference vector to mold
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> new vector to be created
    CLASS(cvector1m), ALLOCATABLE, INTENT(OUT) :: new_cv1m
    !> number of quantities, cv1m%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, cv1m%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_cv1m',iftn,idepth)
    new_nqty=cv1m%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=cv1m%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the base allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_cv1m::new_cv1m)
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_cv1m%skip_elim_interior=cv1m%skip_elim_interior
!-------------------------------------------------------------------------------
!   call the allocation routine
!-------------------------------------------------------------------------------
    SELECT TYPE (new_cv1m)
    TYPE IS (vec_rect_2D_cv1m)
      CALL new_cv1m%alloc(new_pd,cv1m%mx,cv1m%my,new_nqty,cv1m%id)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_cv1m

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_cv1m(cv1m)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) THEN
      ASSOCIATE (arr=>cv1m%arr)
        arr=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      ASSOCIATE (arrh=>cv1m%arrh)
        arrh=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      ASSOCIATE (arrv=>cv1m%arrv)
        arrv=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
      ASSOCIATE (arri=>cv1m%arri)
        arri=0.0_r8
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_cv1m

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_rvec_cv1m(cv1m,rvec,r_i)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_rvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=rvec%skip_elim_interior
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(cv1m%arr))                                               &
          cv1m%arr(:,:,:)=rvec%arr(:,:,:)+(0,1)*AIMAG(cv1m%arr(:,:,:))
        IF (ASSOCIATED(cv1m%arrh))                                              &
          cv1m%arrh(:,:,:,:)=rvec%arrh(:,:,:,:)+(0,1)*AIMAG(cv1m%arrh(:,:,:,:))
        IF (ASSOCIATED(cv1m%arrv))                                              &
          cv1m%arrv(:,:,:,:)=rvec%arrv(:,:,:,:)+(0,1)*AIMAG(cv1m%arrv(:,:,:,:))
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)             &
          cv1m%arri(:,:,:,:)=rvec%arri(:,:,:,:)+(0,1)*AIMAG(cv1m%arri(:,:,:,:))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(cv1m%arr))                                               &
          cv1m%arr(:,:,:)=(0,1)*rvec%arr(:,:,:)+REAL(cv1m%arr(:,:,:))
        IF (ASSOCIATED(cv1m%arrh))                                              &
          cv1m%arrh(:,:,:,:)=(0,1)*rvec%arrh(:,:,:,:)+REAL(cv1m%arrh(:,:,:,:))
        IF (ASSOCIATED(cv1m%arrv))                                              &
          cv1m%arrv(:,:,:,:)=(0,1)*rvec%arrv(:,:,:,:)+REAL(cv1m%arrv(:,:,:,:))
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)             &
          cv1m%arri(:,:,:,:)=(0,1)*rvec%arri(:,:,:,:)+REAL(cv1m%arri(:,:,:,:))
      CASE DEFAULT
        CALL par%nim_stop('assign_rvec_cv1m: '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in assign_rvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_rvec_cv1m

!-------------------------------------------------------------------------------
!* Partially assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_rvec_cv1m(cv1m,rvec,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_rvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=rvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(cv1m%arr))                                               &
          cv1m%arr(v1s:v1e,:,:)=                                                &
            rvec%arr(v2s:v2e,:,:)+(0,1)*AIMAG(cv1m%arr(v1s:v1e,:,:))
        IF (ASSOCIATED(cv1m%arrh))                                              &
          cv1m%arrh(v1s:v1e,:,:,:)=                                             &
            rvec%arrh(v2s:v2e,:,:,:)+(0,1)*AIMAG(cv1m%arrh(v1s:v1e,:,:,:))
        IF (ASSOCIATED(cv1m%arrv))                                              &
          cv1m%arrv(v1s:v1e,:,:,:)=                                             &
            rvec%arrv(v2s:v2e,:,:,:)+(0,1)*AIMAG(cv1m%arrv(v1s:v1e,:,:,:))
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)             &
          cv1m%arri(v1s:v1e,:,:,:)=                                             &
            rvec%arri(v2s:v2e,:,:,:)+(0,1)*AIMAG(cv1m%arri(v1s:v1e,:,:,:))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(cv1m%arr))                                               &
          cv1m%arr(v1s:v1e,:,:)=                                                &
            (0,1)*rvec%arr(v2s:v2e,:,:)+REAL(cv1m%arr(v1s:v1e,:,:))
        IF (ASSOCIATED(cv1m%arrh))                                              &
          cv1m%arrh(v1s:v1e,:,:,:)=                                             &
            (0,1)*rvec%arrh(v2s:v2e,:,:,:)+REAL(cv1m%arrh(v1s:v1e,:,:,:))
        IF (ASSOCIATED(cv1m%arrv))                                              &
          cv1m%arrv(v1s:v1e,:,:,:)=                                             &
            (0,1)*rvec%arrv(v2s:v2e,:,:,:)+REAL(cv1m%arrv(v1s:v1e,:,:,:))
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)             &
          cv1m%arri(v1s:v1e,:,:,:)=                                             &
            (0,1)*rvec%arri(v2s:v2e,:,:,:)+REAL(cv1m%arri(v1s:v1e,:,:,:))
      CASE DEFAULT
        CALL par%nim_stop('assignp_rvec_cv1m: '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in'//                &
                        ' assignp_rvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_rvec_cv1m

!-------------------------------------------------------------------------------
!* Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cv1m_cv1m(cv1m,cv1m2)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cv1m_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=cv1m2%skip_elim_interior
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m)
      IF (ASSOCIATED(cv1m%arr))  cv1m%arr=cv1m2%arr
      IF (ASSOCIATED(cv1m%arrh)) cv1m%arrh=cv1m2%arrh
      IF (ASSOCIATED(cv1m%arrv)) cv1m%arrv=cv1m2%arrv
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)               &
        cv1m%arri=cv1m2%arri
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m2 in'//              &
                        ' assign_cv1m_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cv1m_cv1m

!-------------------------------------------------------------------------------
!* Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cv1m_cv1m(cv1m,cv1m2,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
    cv1m%skip_elim_interior=cv1m2%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m)
      IF (ASSOCIATED(cv1m%arr))                                                 &
        cv1m%arr(v1s:v1e,:,:)=cv1m2%arr(v2s:v2e,:,:)
      IF (ASSOCIATED(cv1m%arrh))                                                &
        cv1m%arrh(v1s:v1e,:,:,:)=cv1m2%arrh(v2s:v2e,:,:,:)
      IF (ASSOCIATED(cv1m%arrv))                                                &
        cv1m%arrv(v1s:v1e,:,:,:)=cv1m2%arrv(v2s:v2e,:,:,:)
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)               &
        cv1m%arri(v1s:v1e,:,:,:)=cv1m2%arri(v2s:v2e,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m2 in'//              &
                        ' assignp_cv1m_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cv1m_cv1m

!-------------------------------------------------------------------------------
!* Assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cvec_cv1m(cv1m,cvec,imode)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=cvec%skip_elim_interior
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp)
      IF (ASSOCIATED(cv1m%arr))  cv1m%arr(:,:,:)=cvec%arr(:,:,:,imode)
      IF (ASSOCIATED(cv1m%arrh)) cv1m%arrh(:,:,:,:)=cvec%arrh(:,:,:,:,imode)
      IF (ASSOCIATED(cv1m%arrv)) cv1m%arrv(:,:,:,:)=cvec%arrv(:,:,:,:,imode)
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)               &
        cv1m%arri(:,:,:,:)=cvec%arri(:,:,:,:,imode)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assign_cvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cvec_cv1m

!-------------------------------------------------------------------------------
!* Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cvec_cv1m(cv1m,cvec,imode,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=cvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp)
      IF (ASSOCIATED(cv1m%arr))                                                 &
        cv1m%arr(v1s:v1e,:,:)=cvec%arr(v2s:v2e,:,:,imode)
      IF (ASSOCIATED(cv1m%arrh))                                                &
        cv1m%arrh(v1s:v1e,:,:,:)=cvec%arrh(v2s:v2e,:,:,:,imode)
      IF (ASSOCIATED(cv1m%arrv))                                                &
        cv1m%arrv(v1s:v1e,:,:,:)=cvec%arrv(v2s:v2e,:,:,:,imode)
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)               &
        cv1m%arri(v1s:v1e,:,:,:)=cvec%arri(v2s:v2e,:,:,:,imode)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assignp_cvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cvec_cv1m

!-------------------------------------------------------------------------------
!* add a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE add_vec_cv1m(cv1m,cv1m2,v1st,v2st,nq,v1fac,v2fac)
    USE convert_type
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1 when v1st or v2st is set
    !> default is all when neither v1st or v2st is set)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    !> factor to multiply vec by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
    !> factor to multiply vec2 by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v2fac

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    COMPLEX(r8) :: v1f,v2f

    CALL timer%start_timer_l2(mod_name,'add_vec_cv1m',iftn,idepth)
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    IF (PRESENT(v1fac)) THEN
      v1f=convert_to_cmplx_r8(v1fac)
    ELSE
      v1f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    IF (PRESENT(v2fac)) THEN
      v2f=convert_to_cmplx_r8(v2fac)
    ELSE
      v2f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m)
      IF (.NOT.PRESENT(v1st).AND..NOT.PRESENT(v2st).AND..NOT.PRESENT(nq)) THEN
        IF (ASSOCIATED(cv1m%arr))  cv1m%arr=v1f*cv1m%arr+v2f*cv1m2%arr
        IF (ASSOCIATED(cv1m%arrh)) cv1m%arrh=v1f*cv1m%arrh+v2f*cv1m2%arrh
        IF (ASSOCIATED(cv1m%arrv)) cv1m%arrv=v1f*cv1m%arrv+v2f*cv1m2%arrv
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)             &
          cv1m%arri=v1f*cv1m%arri+v2f*cv1m2%arri
      ELSE
        IF (ASSOCIATED(cv1m%arr))                                               &
          cv1m%arr(v1s:v1e,:,:)=                                                &
            v1f*cv1m%arr(v1s:v1e,:,:)+v2f*cv1m2%arr(v2s:v2e,:,:)
        IF (ASSOCIATED(cv1m%arrh))                                              &
          cv1m%arrh(v1s:v1e,:,:,:)=                                             &
            v1f*cv1m%arrh(v1s:v1e,:,:,:)+v2f*cv1m2%arrh(v2s:v2e,:,:,:)
        IF (ASSOCIATED(cv1m%arrv))                                              &
          cv1m%arrv(v1s:v1e,:,:,:)=                                             &
            v1f*cv1m%arrv(v1s:v1e,:,:,:)+v2f*cv1m2%arrv(v2s:v2e,:,:,:)
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)             &
          cv1m%arri(v1s:v1e,:,:,:)=                                             &
            v1f*cv1m%arri(v1s:v1e,:,:,:)+v2f*cv1m2%arri(v2s:v2e,:,:,:)
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cvec in add_vec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_vec_cv1m

!-------------------------------------------------------------------------------
!* multiply a vector by a real scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_rsc_cv1m(cv1m,rscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> scalar to multiply
    REAL(r8), INTENT(IN) :: rscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_rsc_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) cv1m%arr=cv1m%arr*rscalar
    IF (ASSOCIATED(cv1m%arrh)) cv1m%arrh=cv1m%arrh*rscalar
    IF (ASSOCIATED(cv1m%arrv)) cv1m%arrv=cv1m%arrv*rscalar
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)                 &
      cv1m%arri=cv1m%arri*rscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_rsc_cv1m

!-------------------------------------------------------------------------------
!* multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_csc_cv1m(cv1m,cscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> scalar to multiply
    COMPLEX(r8), INTENT(IN) :: cscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_csc_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) cv1m%arr=cv1m%arr*cscalar
    IF (ASSOCIATED(cv1m%arrh)) cv1m%arrh=cv1m%arrh*cscalar
    IF (ASSOCIATED(cv1m%arrv)) cv1m%arrv=cv1m%arrv*cscalar
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)                 &
      cv1m%arri=cv1m%arri*cscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_csc_cv1m

!-------------------------------------------------------------------------------
!* multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_int_cv1m(cv1m,iscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> scalar to multiply
    INTEGER(i4), INTENT(IN) :: iscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_int_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) cv1m%arr=cv1m%arr*iscalar
    IF (ASSOCIATED(cv1m%arrh)) cv1m%arrh=cv1m%arrh*iscalar
    IF (ASSOCIATED(cv1m%arrv)) cv1m%arrv=cv1m%arrv*iscalar
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior)                 &
      cv1m%arri=cv1m%arri*iscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_int_cv1m

!-------------------------------------------------------------------------------
!* compute the inf norm
!-------------------------------------------------------------------------------
  SUBROUTINE inf_norm_cv1m(cv1m,infnorm)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> inf norm contributions from the vector (norm_size)
    REAL(r8), INTENT(INOUT) :: infnorm(:)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'inf_norm_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) THEN
      ASSOCIATE (arr=>cv1m%arr)
        infnorm(1)=MAXVAL(ABS(arr))
      END ASSOCIATE
    ELSE
      infnorm(1)=0.
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      ASSOCIATE (arrh=>cv1m%arrh)
        infnorm(2)=MAXVAL(ABS(arrh))
      END ASSOCIATE
    ELSE
      infnorm(2)=0.
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      ASSOCIATE (arrv=>cv1m%arrv)
        infnorm(3)=MAXVAL(ABS(arrv))
      END ASSOCIATE
    ELSE
      infnorm(3)=0.
    ENDIF
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
      ASSOCIATE (arri=>cv1m%arri)
        infnorm(4)=MAXVAL(ABS(arri))
      END ASSOCIATE
    ELSE
      infnorm(4)=0.
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE inf_norm_cv1m

!-------------------------------------------------------------------------------
!* compute the L2 norm squared
!-------------------------------------------------------------------------------
  SUBROUTINE l2_norm2_cv1m(cv1m,l2norm,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> L2 norm contribution from the vector (additive to prior value)
    REAL(r8), INTENT(INOUT) :: l2norm(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'l2_norm2_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   boundary points must be divided by the number of internal
!   representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(cv1m%arr)) THEN
      ASSOCIATE (arr=>cv1m%arr)
        l2norm(1)=SUM(REAL(CONJG(arr)*arr))
      END ASSOCIATE
      ASSOCIATE (arr=>cv1m%arr,vertex=>edge%vertex)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(2)=l2norm(2)+(vertex(iv)%ave_factor-1._r8)*                    &
                       SUM(REAL(CONJG(arr(:,ix,iy))*arr(:,ix,iy)))
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      ASSOCIATE (arrh=>cv1m%arrh)
        l2norm(3)=SUM(REAL(CONJG(arrh)*arrh))
      END ASSOCIATE
      ASSOCIATE (arrh=>cv1m%arrh,segment=>edge%segment)
        DO iv=1,edge%nvert
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            l2norm(4)=l2norm(4)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrh(:,:,ix,iy))*arrh(:,:,ix,iy)))
          ENDIF
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      ASSOCIATE (arrv=>cv1m%arrv)
        l2norm(5)=SUM(REAL(CONJG(arrv)*arrv))
      END ASSOCIATE
      ASSOCIATE (arrv=>cv1m%arrv,segment=>edge%segment)
        DO iv=1,edge%nvert
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (.NOT.segment(iv)%h_side) THEN
            l2norm(6)=l2norm(6)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrv(:,:,ix,iy))*arrv(:,:,ix,iy)))
          ENDIF
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
      ASSOCIATE (arri=>cv1m%arri)
        l2norm(7)=SUM(REAL(CONJG(arri)*arri))
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE l2_norm2_cv1m

!-------------------------------------------------------------------------------
!*  compute the dot product of cv1m and cv1m2
!-------------------------------------------------------------------------------
  SUBROUTINE dot_cv1m(cv1m,cv1m2,dot,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> vector to dot
    CLASS(cvector1m), INTENT(IN) :: cv1m2
    !> dot contribution from the vector (additive to prior value)
    COMPLEX(r8), INTENT(INOUT) :: dot(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   boundary points must be divided by the number of internal
!   representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m)
      IF (ASSOCIATED(cv1m%arr)) THEN
        ASSOCIATE (arr=>cv1m%arr,arr2=>cv1m2%arr)
          dot(1)=SUM(CONJG(arr)*arr2)
        END ASSOCIATE
        ASSOCIATE (arr=>cv1m%arr,arr2=>cv1m2%arr,vertex=>edge%vertex)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(2)=dot(2)+(vertex(iv)%ave_factor-1._r8)*                        &
                         SUM(CONJG(arr(:,ix,iy))*arr2(:,ix,iy))
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        ASSOCIATE (arrh=>cv1m%arrh,arrh2=>cv1m2%arrh)
          dot(3)=SUM(CONJG(arrh)*arrh2)
        END ASSOCIATE
        ASSOCIATE (arrh=>cv1m%arrh,arrh2=>cv1m2%arrh,segment=>edge%segment)
          DO iv=1,edge%nvert
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              dot(4)=dot(4)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrh(:,:,ix,iy))*arrh2(:,:,ix,iy))
            ENDIF
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        ASSOCIATE (arrv=>cv1m%arrv,arrv2=>cv1m2%arrv)
          dot(5)=SUM(CONJG(arrv)*arrv2)
        END ASSOCIATE
        ASSOCIATE (arrv=>cv1m%arrv,arrv2=>cv1m2%arrv,segment=>edge%segment)
          DO iv=1,edge%nvert
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (.NOT.segment(iv)%h_side) THEN
              dot(6)=dot(6)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrv(:,:,ix,iy))*arrv2(:,:,ix,iy))
            ENDIF
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        ASSOCIATE (arri=>cv1m%arri,arri2=>cv1m2%arri)
          dot(7)=SUM(CONJG(arri)*arri2)
        END ASSOCIATE
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for vec2 in dot_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dot_cv1m

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_cv1m(cv1m,integrand)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> integrand array
    COMPLEX(r8), INTENT(IN) :: integrand(:,:,:)

    INTEGER(i4) :: ipol,iv,iy,ix,is,ii
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element into the
!   correct arrays. factors of Jacobian and quadrature weight are already in
!   the test function arrays included in the integrand.
!-------------------------------------------------------------------------------
    iv=1
    IF (ASSOCIATED(cv1m%arr)) THEN
      ipol=1
      DO iy=0,cv1m%my-1
        DO ix=0,cv1m%mx-1
          cv1m%arr(:,ix,iy)=cv1m%arr(:,ix,iy)+integrand(:,ipol,1)
          cv1m%arr(:,ix+1,iy)=cv1m%arr(:,ix+1,iy)+integrand(:,ipol,2)
          cv1m%arr(:,ix,iy+1)=cv1m%arr(:,ix,iy+1)+integrand(:,ipol,3)
          cv1m%arr(:,ix+1,iy+1)=cv1m%arr(:,ix+1,iy+1)+integrand(:,ipol,4)
          ipol=ipol+1
        ENDDO
      ENDDO
      iv=iv+4
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      DO is=1,cv1m%n_side
        ipol=1
        DO iy=0,cv1m%my-1
          DO ix=1,cv1m%mx
            cv1m%arrh(:,is,ix,iy)=cv1m%arrh(:,is,ix,iy)+integrand(:,ipol,iv)
            cv1m%arrh(:,is,ix,iy+1)=cv1m%arrh(:,is,ix,iy+1)                     &
                                        +integrand(:,ipol,iv+1)
            ipol=ipol+1
          ENDDO
        ENDDO
        iv=iv+2
      ENDDO
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      DO is=1,cv1m%n_side
        ipol=1
        DO iy=1,cv1m%my
          DO ix=0,cv1m%mx-1
            cv1m%arrv(:,is,ix,iy)=cv1m%arrv(:,is,ix,iy)+integrand(:,ipol,iv)
            cv1m%arrv(:,is,ix+1,iy)=cv1m%arrv(:,is,ix+1,iy)                     &
                                       +integrand(:,ipol,iv+1)
            ipol=ipol+1
          ENDDO
        ENDDO
        iv=iv+2
      ENDDO
    ENDIF
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
      DO ii=1,cv1m%n_int
        ipol=1
        DO iy=1,cv1m%my
          DO ix=1,cv1m%mx
            cv1m%arri(:,ii,ix,iy)=cv1m%arri(:,ii,ix,iy)+integrand(:,ipol,iv)
            ipol=ipol+1
          ENDDO
        ENDDO
        iv=iv+1
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_cv1m

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_cv1m(cv1m,component,edge,symm,seam_save)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
    !> save the existing boundary data in seam_save
    LOGICAL, OPTIONAL, INTENT(IN) :: seam_save

    COMPLEX(r8), DIMENSION(cv1m%nqty) :: cproj
    COMPLEX(r8), DIMENSION(:,:,:,:), POINTER :: arr4d
    REAL(r8), DIMENSION(cv1m%nqty,cv1m%nqty) :: bcpmat
    INTEGER(i4), DIMENSION(cv1m%nqty,2) :: bciarr
    INTEGER(i4) :: iv,ix,iy,ivp,is,isymm
    !> save the existing boundary data in seam_save
    LOGICAL :: seam_save_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
    IF (PRESENT(seam_save)) THEN
      seam_save_loc=seam_save
    ELSE
      seam_save_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,cv1m%nqty,bciarr)
!-------------------------------------------------------------------------------
!   the bcdir_set routine combines the bciarr information with local
!   surface-normal and tangential unit directions.  mesh vertices
!   are treated first.
!-------------------------------------------------------------------------------
    vert: DO iv=1,edge%nvert
      IF (.NOT.edge%expoint(iv)) CYCLE
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      CALL bcdir_set(cv1m%nqty,bciarr,edge%excorner(iv),isymm,                  &
                     edge%vert_norm(:,iv),edge%vert_tang(:,iv),bcpmat)
      IF (seam_save_loc) THEN
        cproj=MATMUL(bcpmat,cv1m%arr(:,ix,iy))
        cv1m%arr(:,ix,iy)=cv1m%arr(:,ix,iy)-cproj
        edge%vert_csave(1:cv1m%nqty,1,iv)=                                      &
          edge%vert_csave(1:cv1m%nqty,1,iv)+cproj
      ELSE
        cproj=MATMUL(bcpmat,cv1m%arr(:,ix,iy))
        cv1m%arr(:,ix,iy)=cv1m%arr(:,ix,iy)-cproj
      ENDIF
    ENDDO vert
!-------------------------------------------------------------------------------
!   side-centered nodes.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(cv1m%arrh)) THEN
      seg: DO iv=1,edge%nvert
        ivp=iv-1
        IF (ivp==0) ivp=edge%nvert
        IF (.NOT.(edge%expoint(iv).AND.edge%expoint(ivp))) CYCLE seg
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          arr4d=>cv1m%arrh
        ELSE
          arr4d=>cv1m%arrv
        ENDIF
        IF (seam_save_loc) THEN
          DO is=1,cv1m%n_side
            CALL bcdir_set(cv1m%nqty,bciarr,.false.,isymm,                      &
                           edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),bcpmat)
            cproj=MATMUL(bcpmat,arr4d(:,is,ix,iy))
            arr4d(:,is,ix,iy)=arr4d(:,is,ix,iy)-cproj
            edge%seg_csave(1:cv1m%nqty,is,1,iv)=                                &
              edge%seg_csave(1:cv1m%nqty,is,1,iv)+cproj
          ENDDO
        ELSE
          DO is=1,cv1m%n_side
            CALL bcdir_set(cv1m%nqty,bciarr,.false.,isymm,                      &
                           edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),bcpmat)
            cproj=MATMUL(bcpmat,arr4d(:,is,ix,iy))
            arr4d(:,is,ix,iy)=arr4d(:,is,ix,iy)-cproj
          ENDDO
        ENDIF
      ENDDO seg
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_cv1m

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_cv1m(cv1m,edge,flag)
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> flag
    CHARACTER(*), INTENT(IN) :: flag

    INTEGER(i4) :: iv,ivp,ix,iy,iside,ivec,nvec,imn1
    REAL(r8), DIMENSION(cv1m%nqty,1) :: mult
    COMPLEX(r8), DIMENSION(:,:,:,:), POINTER :: arr4d
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different Fourier components
!   of a vector.  for a scalar quantity, n>0 are set to 0.  for a
!   vector, thd following components are set to 0
!   n=0:  r and phi
!   n=1:  z
!   n>1:  r, z, and phi
!
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!-------------------------------------------------------------------------------
    mult=0
    imn1=0
    nvec=0
    SELECT CASE(cv1m%nqty)
    CASE(1,2,4,5)          !   scalars
      IF (cv1m%fcomp==0) THEN
        mult(:,1)=1
      ENDIF
    CASE(3,6,9,12,15,18)   !   3-vectors
      nvec=cv1m%nqty/3
      DO ivec=0,nvec-1
        IF (cv1m%fcomp==0) THEN
          mult(3*ivec+2,1)=1
        ELSEIF (cv1m%fcomp==1) THEN
          mult(3*ivec+1,1)=1
          IF (flag=='cyl_vec') imn1=cv1m%fcomp
          IF (flag=='init_cyl_vec') mult(3*ivec+3,1)=1
        ENDIF
      ENDDO
    CASE DEFAULT
      CALL par%nim_stop("vec_rect_2D::regularity_cv1m"//                        &
                        " inconsistent # of components.")
    END SELECT
!-------------------------------------------------------------------------------
!   loop over block border elements and apply mult to the vertices
!   at R=0.  also combine rhs for n=1 r and phi comps.
!-------------------------------------------------------------------------------
    vert: DO iv=1,edge%nvert
      IF (.NOT.edge%r0point(iv)) CYCLE
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      IF (imn1/=0) THEN
        DO ivec=0,nvec-1
          cv1m%arr(3*ivec+1,ix,iy)=cv1m%arr(3*ivec+1,ix,iy)                     &
                                   -(0,1)*cv1m%arr(3*ivec+3,ix,iy)
        ENDDO
      ENDIF
      cv1m%arr(:,ix,iy)=cv1m%arr(:,ix,iy)*mult(:,1)
      IF (ASSOCIATED(cv1m%arrh)) THEN
        ivp=iv-1
        IF (ivp==0) ivp=edge%nvert
        IF (.NOT.edge%r0point(ivp)) CYCLE
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          arr4d=>cv1m%arrh
        ELSE
          arr4d=>cv1m%arrv
        ENDIF
        IF (imn1/=0) THEN
          DO ivec=0,nvec-1
            arr4d(3*ivec+1,:,ix,iy)=arr4d(3*ivec+1,:,ix,iy)                     &
                                    -(0,1)*arr4d(3*ivec+3,:,ix,iy)
          ENDDO
        ENDIF
        DO iside=1,cv1m%n_side
          arr4d(:,iside,ix,iy)=arr4d(:,iside,ix,iy)*mult(:,1)
        ENDDO
      ENDIF
    ENDDO vert
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_cv1m

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_cv1m(cv1m,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_cv1m',iftn,idepth)
    DO iv=1,cv1m%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cv1m%mx+1,cv1m%mx+cv1m%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cv1m%mx+cv1m%my+1,2*cv1m%mx+cv1m%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*cv1m%mx+cv1m%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_cv1m

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_cv1m(cv1m,edge,nqty,n_side,do_avg)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> apply edge average if true
    LOGICAL, OPTIONAL, INTENT(IN) :: do_avg

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns
    LOGICAL :: do_avg_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cv1m%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cv1m%n_side
    ENDIF
    IF (PRESENT(do_avg)) THEN
      do_avg_loc=do_avg
    ELSE
      do_avg_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%nmodes_loaded=1_i4
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vert_cin(1:nq,iv)=                                                   &
        cv1m%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  load side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            edge%seg_cin(iq:nq+iq-1,iv)=cv1m%arrh(1:nq,is,ix,iy)
            iq=iq+nq
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            edge%seg_cin(iq:nq+iq-1,iv)=cv1m%arrv(1:nq,is,ix,iy)
            iq=iq+nq
          ENDDO
        ENDIF
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   apply average factor
!-------------------------------------------------------------------------------
    IF (do_avg_loc) THEN
      DO iv=1,edge%nvert
        edge%vert_cin(1:nq,iv)=                                                 &
          edge%vert_cin(1:nq,iv)*edge%vertex(iv)%ave_factor
        IF (ns/=0) THEN
          edge%seg_cin(1:nq*ns,iv)=                                             &
            edge%seg_cin(1:nq*ns,iv)*edge%segment(iv)%ave_factor
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_cv1m

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_cv1m(cv1m,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      cv1m%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))=         &
        edge%vert_cout(1:nq,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  unload side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            cv1m%arrh(1:nq,is,ix,iy)=edge%seg_cout(iq:nq+iq-1,iv)
            iq=iq+nq
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            cv1m%arrv(1:nq,is,ix,iy)=edge%seg_cout(iq:nq+iq-1,iv)
            iq=iq+nq
          ENDDO
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_cv1m

!-------------------------------------------------------------------------------
!* load a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_save_cv1m(cv1m,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_save_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cv1m%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cv1m%n_side
    ENDIF
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vert_csave(1:nq,1,iv)=                                               &
        cv1m%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          edge%seg_csave(1:nq,:,1,iv)=cv1m%arrh(1:nq,:,ix,iy)
        ELSE
          edge%seg_csave(1:nq,:,1,iv)=cv1m%arrv(1:nq,:,ix,iy)
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_save_cv1m

!-------------------------------------------------------------------------------
!* unload a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_add_save_cv1m(cv1m,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be add to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> edge to add from
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_add_save_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cv1m%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cv1m%n_side
    ENDIF
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      cv1m%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))=         &
        cv1m%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2))+       &
        edge%vert_csave(1:nq,1,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          cv1m%arrh(1:nq,:,ix,iy)=cv1m%arrh(1:nq,:,ix,iy)+                      &
            edge%seg_csave(1:nq,:,1,iv)
        ELSE
          cv1m%arrv(1:nq,:,ix,iy)=cv1m%arrv(1:nq,:,ix,iy)+                      &
            edge%seg_csave(1:nq,:,1,iv)
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_add_save_cv1m

!-------------------------------------------------------------------------------
!> Assume vector data represents the diagonal of a matrix and apply a matrix
!  multiply.
!-------------------------------------------------------------------------------
  SUBROUTINE apply_diag_matvec_cv1m(cv1m,input_vec,output_vec)
    IMPLICIT NONE

    !> vector that represents a diagonal matrix
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> vector to multiply
    CLASS(cvector1m), INTENT(IN) :: input_vec
    !> output vector = cv1m (matrix diagonal) dot input_vec
    CLASS(cvector1m), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_diag_matvec_cv1m',iftn,idepth)
    SELECT TYPE (input_vec)
    TYPE IS (vec_rect_2D_cv1m)
      SELECT TYPE (output_vec)
      TYPE IS (vec_rect_2D_cv1m)
        IF (ASSOCIATED(cv1m%arr)) THEN
          output_vec%arr=cv1m%arr*input_vec%arr
        ENDIF
        IF (ASSOCIATED(cv1m%arrh)) THEN
          output_vec%arrh=cv1m%arrh*input_vec%arrh
        ENDIF
        IF (ASSOCIATED(cv1m%arrv)) THEN
          output_vec%arrv=cv1m%arrv*input_vec%arrv
        ENDIF
        IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
          output_vec%arri=cv1m%arri*input_vec%arri
        ENDIF
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_cv1m for output_vec'            &
                          //' in apply_diag_matvec_cv1m')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for input_vec'               &
                        //' in apply_diag_matvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_diag_matvec_cv1m

!-------------------------------------------------------------------------------
!> Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
  SUBROUTINE invert_cv1m(cv1m)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'invert_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) THEN
      cv1m%arr=1._r8/cv1m%arr
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      cv1m%arrh=1._r8/cv1m%arrh
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      cv1m%arrv=1._r8/cv1m%arrv
    ENDIF
    IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
      cv1m%arri=1._r8/cv1m%arri
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE invert_cv1m

!-------------------------------------------------------------------------------
!* Assign to the vector for testing
!-------------------------------------------------------------------------------
  SUBROUTINE assign_for_testing_cv1m(cv1m,operation,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m), INTENT(INOUT) :: cv1m
    !> operation: 'constant' or 'index'
    CHARACTER(*), INTENT(IN) :: operation
    !> optional constant value for operation='constant'
    COMPLEX(r8), INTENT(IN), OPTIONAL :: const

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    COMPLEX(r8) :: fac
    INTEGER(i4) :: ii1,ii2,ii3,ii4,ind

    CALL timer%start_timer_l2(mod_name,'assign_for_testing_cv1m',iftn,idepth)
    fac=1._r8
    IF (PRESENT(const)) fac=const
    SELECT CASE(operation)
    CASE("constant")
      IF (ASSOCIATED(cv1m%arr)) THEN
        ASSOCIATE (arr=>cv1m%arr)
          arr=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        ASSOCIATE (arrh=>cv1m%arrh)
          arrh=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        ASSOCIATE (arrv=>cv1m%arrv)
          arrv=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        ASSOCIATE (arri=>cv1m%arri)
          arri=fac
        END ASSOCIATE
      ENDIF
    CASE("index")
      IF (ASSOCIATED(cv1m%arr)) THEN
        ASSOCIATE (arr=>cv1m%arr)
          DO ii1=LBOUND(arr,1),UBOUND(arr,1)
            DO ii2=LBOUND(arr,2),UBOUND(arr,2)
              DO ii3=LBOUND(arr,3),UBOUND(arr,3)
                ind=ii1*SIZE(arr,2)*SIZE(arr,3)+ii2*SIZE(arr,3)+ii3
                arr(ii1,ii2,ii3)=ind
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        ASSOCIATE (arrh=>cv1m%arrh)
          DO ii1=LBOUND(arrh,1),UBOUND(arrh,1)
            DO ii2=LBOUND(arrh,2),UBOUND(arrh,2)
              DO ii3=LBOUND(arrh,3),UBOUND(arrh,3)
                DO ii4=LBOUND(arrh,4),UBOUND(arrh,4)
                  ind=ii1*SIZE(arrh,2)*SIZE(arrh,3)*SIZE(arrh,4)+               &
                      ii2*SIZE(arrh,3)*SIZE(arrh,4)+ii3*SIZE(arrh,4)+ii4
                  arrh(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        ASSOCIATE (arrv=>cv1m%arrv)
          DO ii1=LBOUND(arrv,1),UBOUND(arrv,1)
            DO ii2=LBOUND(arrv,2),UBOUND(arrv,2)
              DO ii3=LBOUND(arrv,3),UBOUND(arrv,3)
                DO ii4=LBOUND(arrv,4),UBOUND(arrv,4)
                  ind=ii1*SIZE(arrv,2)*SIZE(arrv,3)*SIZE(arrv,4)+               &
                      ii2*SIZE(arrv,3)*SIZE(arrv,4)+ii3*SIZE(arrv,4)+ii4
                  arrv(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        ASSOCIATE (arri=>cv1m%arri)
          DO ii1=LBOUND(arri,1),UBOUND(arri,1)
            DO ii2=LBOUND(arri,2),UBOUND(arri,2)
              DO ii3=LBOUND(arri,3),UBOUND(arri,3)
                DO ii4=LBOUND(arri,4),UBOUND(arri,4)
                  ind=ii1*SIZE(arri,2)*SIZE(arri,3)*SIZE(arri,4)+               &
                      ii2*SIZE(arri,3)*SIZE(arri,4)+ii3*SIZE(arri,4)+ii4
                  arri(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop('Expected operation=constant or index'                  &
                    //' in assign_for_testing_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_for_testing_cv1m

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
  LOGICAL FUNCTION test_if_equal_cv1m(cv1m,cv1m2) RESULT(output)
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m), INTENT(IN) :: cv1m
    !> vector to test against
    CLASS(cvector1m), INTENT(IN) :: cv1m2

    INTEGER(i4) :: shape3a(3),shape3b(3),shape4a(4),shape4b(4)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'test_if_equal_cv1m',iftn,idepth)
    output=.TRUE.
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m)
      IF (ASSOCIATED(cv1m%arr)) THEN
        shape3a=SHAPE(cv1m%arr)
        shape3b=SHAPE(cv1m2%arr)
        IF (.NOT.ALL(shape3a == shape3b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arr == cv1m2%arr)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arr)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        shape4a=SHAPE(cv1m%arrh)
        shape4b=SHAPE(cv1m2%arrh)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arrh == cv1m2%arrh)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arrh)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        shape4a=SHAPE(cv1m%arrv)
        shape4b=SHAPE(cv1m2%arrv)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arrv == cv1m2%arrv)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arrv)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        shape4a=SHAPE(cv1m%arri)
        shape4b=SHAPE(cv1m2%arri)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arri == cv1m2%arri)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arri)) THEN
        output=.FALSE.
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m2 in'//              &
                        ' test_if_equal_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION test_if_equal_cv1m
