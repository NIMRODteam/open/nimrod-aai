!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_comp included in vec_rect_2D.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a complex single-mode vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_comp(cvec,poly_degree,mx,my,nqty,nmodes,id)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> number of Fourier modes
    INTEGER(i4), INTENT(IN) :: nmodes
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    cvec%nqty=nqty
    cvec%mx=mx
    cvec%my=my
    cvec%n_side=poly_degree-1
    cvec%n_int=(poly_degree-1)**2
    cvec%u_ndof=(poly_degree+1)**2
    cvec%pd=poly_degree
    cvec%nel=mx*my
    cvec%ndim=2
    cvec%nmodes=nmodes
    cvec%id=id
    cvec%inf_norm_size=4
    cvec%l2_dot_size=7
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(cvec%arr(nqty,0:mx,0:my,nmodes))
      NULLIFY(cvec%arri,cvec%arrh,cvec%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(cvec%arr(nqty,0:mx,0:my,nmodes))
      ALLOCATE(cvec%arrh(nqty,poly_degree-1,1:mx,0:my,nmodes))
      ALLOCATE(cvec%arrv(nqty,poly_degree-1,0:mx,1:my,nmodes))
      ALLOCATE(cvec%arri(nqty,(poly_degree-1)**2,1:mx,1:my,nmodes))
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(cvec%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(cvec%arr)+SIZEOF(cvec%arrh)+SIZEOF(cvec%arrv)              &
             +SIZEOF(cvec%arri)+SIZEOF(cvec),i4)
      CALL memlogger%update(cvec%mem_id,'cvec'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_comp

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_comp(cvec)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) THEN
      DEALLOCATE(cvec%arr)
      NULLIFY(cvec%arr)
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      DEALLOCATE(cvec%arrh)
      NULLIFY(cvec%arrh)
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      DEALLOCATE(cvec%arrv)
      NULLIFY(cvec%arrv)
    ENDIF
    IF (ASSOCIATED(cvec%arri)) THEN
      DEALLOCATE(cvec%arri)
      NULLIFY(cvec%arri)
    ENDIF
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(cvec%mem_id,'cvec'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_comp

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_comp(cvec,new_cvec,nqty,pd,nmodes)
    IMPLICIT NONE

    !> reference cvector to mold
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> new cvector to be created
    CLASS(cvector), ALLOCATABLE, INTENT(OUT) :: new_cvec
    !> number of quantities, cvec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, cvec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
    !> number of Fourier components, cvec%nmodes is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nmodes

    INTEGER(i4) :: new_nqty,new_pd,new_nmodes
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_comp',iftn,idepth)
    new_nqty=cvec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=cvec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
    new_nmodes=cvec%nmodes
    IF (PRESENT(nmodes)) THEN
      new_nmodes=nmodes
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_comp::new_cvec)
    SELECT TYPE (new_cvec)
    TYPE IS (vec_rect_2D_comp)
      CALL new_cvec%alloc(new_pd,cvec%mx,cvec%my,new_nqty,new_nmodes,cvec%id)
    END SELECT
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_cvec%skip_elim_interior=cvec%skip_elim_interior
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_comp

!-------------------------------------------------------------------------------
!* Create a new cvector1m from this cvector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_cv1m_comp(cvec,new_cv1m,nqty,pd)
    IMPLICIT NONE

    !> reference cvector to mold
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> new cvector1m to be created
    CLASS(cvector1m), ALLOCATABLE, INTENT(OUT) :: new_cv1m
    !> number of quantities, cvec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, cvec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd

    new_nqty=cvec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=cvec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_cv1m::new_cv1m)
    SELECT TYPE (new_cv1m)
    TYPE IS (vec_rect_2D_cv1m)
      CALL new_cv1m%alloc(new_pd,cvec%mx,cvec%my,new_nqty,cvec%id)
    END SELECT
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_cv1m%skip_elim_interior=cvec%skip_elim_interior
  END SUBROUTINE alloc_with_mold_cv1m_comp

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_comp(cvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) THEN
      ASSOCIATE (arr=>cvec%arr)
        arr=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      ASSOCIATE (arrh=>cvec%arrh)
        arrh=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      ASSOCIATE (arrv=>cvec%arrv)
        arrv=0.0_r8
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
      ASSOCIATE (arri=>cvec%arri)
        arri=0.0_r8
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_comp

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_rvec_comp(cvec,rvec,imode,r_i)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_rvec_comp',iftn,idepth)
    cvec%skip_elim_interior=rvec%skip_elim_interior
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(cvec%arr))                                               &
          cvec%arr(:,:,:,imode)=                                                &
            rvec%arr(:,:,:)+(0,1)*AIMAG(cvec%arr(:,:,:,imode))
        IF (ASSOCIATED(cvec%arrh))                                              &
          cvec%arrh(:,:,:,:,imode)=                                             &
            rvec%arrh(:,:,:,:)+(0,1)*AIMAG(cvec%arrh(:,:,:,:,imode))
        IF (ASSOCIATED(cvec%arrv))                                              &
          cvec%arrv(:,:,:,:,imode)=                                             &
            rvec%arrv(:,:,:,:)+(0,1)*AIMAG(cvec%arrv(:,:,:,:,imode))
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)             &
          cvec%arri(:,:,:,:,imode)=                                             &
            rvec%arri(:,:,:,:)+(0,1)*AIMAG(cvec%arri(:,:,:,:,imode))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(cvec%arr))                                               &
          cvec%arr(:,:,:,imode)=                                                &
            (0,1)*rvec%arr(:,:,:)+REAL(cvec%arr(:,:,:,imode))
        IF (ASSOCIATED(cvec%arrh))                                              &
          cvec%arrh(:,:,:,:,imode)=                                             &
            (0,1)*rvec%arrh(:,:,:,:)+REAL(cvec%arrh(:,:,:,:,imode))
        IF (ASSOCIATED(cvec%arrv))                                              &
          cvec%arrv(:,:,:,:,imode)=                                             &
            (0,1)*rvec%arrv(:,:,:,:)+REAL(cvec%arrv(:,:,:,:,imode))
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)             &
          cvec%arri(:,:,:,:,imode)=                                             &
            (0,1)*rvec%arri(:,:,:,:)+REAL(cvec%arri(:,:,:,:,imode))
      CASE DEFAULT
        CALL par%nim_stop('assign_rvec_comp: '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in assign_rvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_rvec_comp

!-------------------------------------------------------------------------------
!* Partially assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_rvec_comp(cvec,rvec,imode,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_rvec_comp',iftn,idepth)
    cvec%skip_elim_interior=rvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real)
      SELECT CASE(r_i)
      CASE ('real','REAL')
        IF (ASSOCIATED(cvec%arr))                                               &
          cvec%arr(v1s:v1e,:,:,imode)=                                          &
            rvec%arr(v2s:v2e,:,:)+(0,1)*AIMAG(cvec%arr(v1s:v1e,:,:,imode))
        IF (ASSOCIATED(cvec%arrh))                                              &
          cvec%arrh(v1s:v1e,:,:,:,imode)=                                       &
            rvec%arrh(v2s:v2e,:,:,:)+(0,1)*AIMAG(cvec%arrh(v1s:v1e,:,:,:,imode))
        IF (ASSOCIATED(cvec%arrv))                                              &
          cvec%arrv(v1s:v1e,:,:,:,imode)=                                       &
            rvec%arrv(v2s:v2e,:,:,:)+(0,1)*AIMAG(cvec%arrv(v1s:v1e,:,:,:,imode))
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)             &
          cvec%arri(v1s:v1e,:,:,:,imode)=                                       &
            rvec%arri(v2s:v2e,:,:,:)+(0,1)*AIMAG(cvec%arri(v1s:v1e,:,:,:,imode))
      CASE ('imag','IMAG')
        IF (ASSOCIATED(cvec%arr))                                               &
          cvec%arr(v1s:v1e,:,:,imode)=                                          &
            (0,1)*rvec%arr(v2s:v2e,:,:)+REAL(cvec%arr(v1s:v1e,:,:,imode))
        IF (ASSOCIATED(cvec%arrh))                                              &
          cvec%arrh(v1s:v1e,:,:,:,imode)=                                       &
            (0,1)*rvec%arrh(v2s:v2e,:,:,:)+REAL(cvec%arrh(v1s:v1e,:,:,:,imode))
        IF (ASSOCIATED(cvec%arrv))                                              &
          cvec%arrv(v1s:v1e,:,:,:,imode)=                                       &
            (0,1)*rvec%arrv(v2s:v2e,:,:,:)+REAL(cvec%arrv(v1s:v1e,:,:,:,imode))
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)             &
          cvec%arri(v1s:v1e,:,:,:,imode)=                                       &
            (0,1)*rvec%arri(v2s:v2e,:,:,:)+REAL(cvec%arri(v1s:v1e,:,:,:,imode))
      CASE DEFAULT
        CALL par%nim_stop('assignp_rvec_comp: '//r_i//' flag not recognized.')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in'//                &
                        ' assignp_rvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_rvec_comp

!-------------------------------------------------------------------------------
!* Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cv1m_comp(cvec,cv1m,imode)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cv1m_comp',iftn,idepth)
    cvec%skip_elim_interior=cv1m%skip_elim_interior
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m)
      IF (ASSOCIATED(cvec%arr))  cvec%arr(:,:,:,imode)=cv1m%arr(:,:,:)
      IF (ASSOCIATED(cvec%arrh)) cvec%arrh(:,:,:,:,imode)=cv1m%arrh(:,:,:,:)
      IF (ASSOCIATED(cvec%arrv)) cvec%arrv(:,:,:,:,imode)=cv1m%arrv(:,:,:,:)
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)               &
        cvec%arri(:,:,:,:,imode)=cv1m%arri(:,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for vec in assign_cv1m_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cv1m_comp

!-------------------------------------------------------------------------------
!* Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cv1m_comp(cvec,cv1m,imode,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> mode to assign to
    INTEGER(i4), INTENT(IN) :: imode
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cv1m_comp',iftn,idepth)
    cvec%skip_elim_interior=cv1m%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m)
      IF (ASSOCIATED(cvec%arr))                                                 &
        cvec%arr(v1s:v1e,:,:,imode)=cv1m%arr(v2s:v2e,:,:)
      IF (ASSOCIATED(cvec%arrh))                                                &
        cvec%arrh(v1s:v1e,:,:,:,imode)=cv1m%arrh(v2s:v2e,:,:,:)
      IF (ASSOCIATED(cvec%arrv))                                                &
        cvec%arrv(v1s:v1e,:,:,:,imode)=cv1m%arrv(v2s:v2e,:,:,:)
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)               &
        cvec%arri(v1s:v1e,:,:,:,imode)=cv1m%arri(v2s:v2e,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for vec in'//                &
                        ' assignp_cv1m_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cv1m_comp

!-------------------------------------------------------------------------------
!* Assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cvec_comp(cvec,cvec2)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cvec_comp',iftn,idepth)
    cvec%skip_elim_interior=cvec2%skip_elim_interior
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp)
      IF (ASSOCIATED(cvec%arr))  cvec%arr=cvec2%arr
      IF (ASSOCIATED(cvec%arrh)) cvec%arrh=cvec2%arrh
      IF (ASSOCIATED(cvec%arrv)) cvec%arrv=cvec2%arrv
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)               &
        cvec%arri=cvec2%arri
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assign_cvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cvec_comp

!-------------------------------------------------------------------------------
!* Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cvec_comp(cvec,cvec2,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cvec_comp',iftn,idepth)
    cvec%skip_elim_interior=cvec2%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp)
      IF (ASSOCIATED(cvec%arr))                                                 &
        cvec%arr(v1s:v1e,:,:,:)=cvec2%arr(v2s:v2e,:,:,:)
      IF (ASSOCIATED(cvec%arrh))                                                &
        cvec%arrh(v1s:v1e,:,:,:,:)=cvec2%arrh(v2s:v2e,:,:,:,:)
      IF (ASSOCIATED(cvec%arrv))                                                &
        cvec%arrv(v1s:v1e,:,:,:,:)=cvec2%arrv(v2s:v2e,:,:,:,:)
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)               &
        cvec%arri(v1s:v1e,:,:,:,:)=cvec2%arri(v2s:v2e,:,:,:,:)
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assignp_cvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cvec_comp

!-------------------------------------------------------------------------------
!* add a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE add_vec_comp(cvec,cvec2,v1st,v2st,nq,v1fac,v2fac)
    USE convert_type
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1 when v1st or v2st is set
    !> default is all when neither v1st or v2st is set)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    !> factor to multiply vec by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
    !> factor to multiply vec2 by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v2fac

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    COMPLEX(r8) :: v1f,v2f

    CALL timer%start_timer_l2(mod_name,'add_vec_comp',iftn,idepth)
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    IF (PRESENT(v1fac)) THEN
      v1f=convert_to_cmplx_r8(v1fac)
    ELSE
      v1f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    IF (PRESENT(v2fac)) THEN
      v2f=convert_to_cmplx_r8(v2fac)
    ELSE
      v2f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp)
      IF (.NOT.PRESENT(v1st).AND..NOT.PRESENT(v2st).AND..NOT.PRESENT(nq)) THEN
        IF (ASSOCIATED(cvec%arr))  cvec%arr=v1f*cvec%arr+v2f*cvec2%arr
        IF (ASSOCIATED(cvec%arrh)) cvec%arrh=v1f*cvec%arrh+v2f*cvec2%arrh
        IF (ASSOCIATED(cvec%arrv)) cvec%arrv=v1f*cvec%arrv+v2f*cvec2%arrv
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)             &
          cvec%arri=v1f*cvec%arri+v2f*cvec2%arri
      ELSE
        IF (ASSOCIATED(cvec%arr))                                               &
          cvec%arr(v1s:v1e,:,:,:)=                                              &
            v1f*cvec%arr(v1s:v1e,:,:,:)+v2f*cvec2%arr(v2s:v2e,:,:,:)
        IF (ASSOCIATED(cvec%arrh))                                              &
          cvec%arrh(v1s:v1e,:,:,:,:)=                                           &
            v1f*cvec%arrh(v1s:v1e,:,:,:,:)+v2f*cvec2%arrh(v2s:v2e,:,:,:,:)
        IF (ASSOCIATED(cvec%arrv))                                              &
          cvec%arrv(v1s:v1e,:,:,:,:)=                                           &
            v1f*cvec%arrv(v1s:v1e,:,:,:,:)+v2f*cvec2%arrv(v2s:v2e,:,:,:,:)
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)             &
          cvec%arri(v1s:v1e,:,:,:,:)=                                           &
            v1f*cvec%arri(v1s:v1e,:,:,:,:)+v2f*cvec2%arri(v2s:v2e,:,:,:,:)
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec2 in add_vec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_vec_comp

!-------------------------------------------------------------------------------
!* multiply a vector by a real scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_rsc_comp(cvec,rscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> scalar to multiply
    REAL(r8), INTENT(IN) :: rscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_rsc_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) cvec%arr=cvec%arr*rscalar
    IF (ASSOCIATED(cvec%arrh)) cvec%arrh=cvec%arrh*rscalar
    IF (ASSOCIATED(cvec%arrv)) cvec%arrv=cvec%arrv*rscalar
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)                 &
      cvec%arri=cvec%arri*rscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_rsc_comp

!-------------------------------------------------------------------------------
!* multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_csc_comp(cvec,cscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> scalar to multiply
    COMPLEX(r8), INTENT(IN) :: cscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_csc_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) cvec%arr=cvec%arr*cscalar
    IF (ASSOCIATED(cvec%arrh)) cvec%arrh=cvec%arrh*cscalar
    IF (ASSOCIATED(cvec%arrv)) cvec%arrv=cvec%arrv*cscalar
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)                 &
      cvec%arri=cvec%arri*cscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_csc_comp

!-------------------------------------------------------------------------------
!* multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_int_comp(cvec,iscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> scalar to multiply
    INTEGER(i4), INTENT(IN) :: iscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_int_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) cvec%arr=cvec%arr*iscalar
    IF (ASSOCIATED(cvec%arrh)) cvec%arrh=cvec%arrh*iscalar
    IF (ASSOCIATED(cvec%arrv)) cvec%arrv=cvec%arrv*iscalar
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior)                 &
      cvec%arri=cvec%arri*iscalar
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_int_comp

!-------------------------------------------------------------------------------
!* compute the inf norm
!-------------------------------------------------------------------------------
  SUBROUTINE inf_norm_comp(cvec,infnorm)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> inf norm contributions from the vector (norm_size)
    REAL(r8), INTENT(INOUT) :: infnorm(:)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'inf_norm_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) THEN
      ASSOCIATE (arr=>cvec%arr)
        infnorm(1)=MAXVAL(ABS(arr))
      END ASSOCIATE
    ELSE
      infnorm(1)=0.
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      ASSOCIATE (arrh=>cvec%arrh)
        infnorm(2)=MAXVAL(ABS(arrh))
      END ASSOCIATE
    ELSE
      infnorm(2)=0.
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      ASSOCIATE (arrv=>cvec%arrv)
        infnorm(3)=MAXVAL(ABS(arrv))
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
      ASSOCIATE (arri=>cvec%arri)
        infnorm(4)=MAXVAL(ABS(arri))
      END ASSOCIATE
    ELSE
      infnorm(4)=0.
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE inf_norm_comp

!-------------------------------------------------------------------------------
!* compute the L2 norm squared squared
!-------------------------------------------------------------------------------
  SUBROUTINE l2_norm2_comp(cvec,l2norm,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> L2 norm contribution from the vector (additive to prior value)
    REAL(r8), INTENT(INOUT) :: l2norm(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'l2_norm2_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   boundary points must be divided by the number of internal
!   representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(cvec%arr)) THEN
      ASSOCIATE (arr=>cvec%arr)
        l2norm(1)=SUM(REAL(CONJG(arr)*arr))
      END ASSOCIATE
      ASSOCIATE (arr=>cvec%arr,vertex=>edge%vertex)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(2)=l2norm(2)+(vertex(iv)%ave_factor-1._r8)*                    &
                       SUM(REAL(CONJG(arr(:,ix,iy,:))*arr(:,ix,iy,:)))
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      ASSOCIATE (arrh=>cvec%arrh)
        l2norm(3)=SUM(REAL(CONJG(arrh)*arrh))
      END ASSOCIATE
      ASSOCIATE (arrh=>cvec%arrh,segment=>edge%segment)
        DO iv=1,edge%nvert
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            l2norm(4)=l2norm(4)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrh(:,:,ix,iy,:))*arrh(:,:,ix,iy,:)))
          ENDIF
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      ASSOCIATE (arrv=>cvec%arrv)
        l2norm(5)=SUM(REAL(CONJG(arrv)*arrv))
      END ASSOCIATE
      ASSOCIATE (arrv=>cvec%arrv,segment=>edge%segment)
        DO iv=1,edge%nvert
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (.NOT.segment(iv)%h_side) THEN
            l2norm(6)=l2norm(6)+(segment(iv)%ave_factor-1._r8)                  &
                      *SUM(REAL(CONJG(arrv(:,:,ix,iy,:))*arrv(:,:,ix,iy,:)))
          ENDIF
        ENDDO
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
      ASSOCIATE (arri=>cvec%arri)
        l2norm(7)=SUM(REAL(CONJG(arri)*arri))
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE l2_norm2_comp

!-------------------------------------------------------------------------------
!*  compute the dot product of cvec and cvec2
!-------------------------------------------------------------------------------
  SUBROUTINE dot_comp(cvec,cvec2,dot,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> vector to dot
    CLASS(cvector), INTENT(IN) :: cvec2
    !> dot contribution from the vector (additive to prior value)
    COMPLEX(r8), INTENT(INOUT) :: dot(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   boundary points must be divided by the number of internal
!   representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp)
      IF (ASSOCIATED(cvec%arr)) THEN
        ASSOCIATE (arr=>cvec%arr,arr2=>cvec2%arr)
          dot(1)=SUM(CONJG(arr)*arr2)
        END ASSOCIATE
        ASSOCIATE (arr=>cvec%arr,arr2=>cvec2%arr,vertex=>edge%vertex)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(2)=dot(2)+(vertex(iv)%ave_factor-1._r8)*                        &
                         SUM(CONJG(arr(:,ix,iy,:))*arr2(:,ix,iy,:))
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        ASSOCIATE (arrh=>cvec%arrh,arrh2=>cvec2%arrh)
          dot(3)=SUM(CONJG(arrh)*arrh2)
        END ASSOCIATE
        ASSOCIATE (arrh=>cvec%arrh,arrh2=>cvec2%arrh,segment=>edge%segment)
          DO iv=1,edge%nvert
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              dot(4)=dot(4)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrh(:,:,ix,iy,:))*arrh2(:,:,ix,iy,:))
            ENDIF
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        ASSOCIATE (arrv=>cvec%arrv,arrv2=>cvec2%arrv)
          dot(5)=SUM(CONJG(arrv)*arrv2)
        END ASSOCIATE
        ASSOCIATE (arrv=>cvec%arrv,arrv2=>cvec2%arrv,segment=>edge%segment)
          DO iv=1,edge%nvert
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (.NOT.segment(iv)%h_side) THEN
              dot(6)=dot(6)+(segment(iv)%ave_factor-1._r8)                      &
                        *SUM(CONJG(arrv(:,:,ix,iy,:))*arrv2(:,:,ix,iy,:))
            ENDIF
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>cvec%arri,arri2=>cvec2%arri)
          dot(7)=SUM(CONJG(arri)*arri2)
        END ASSOCIATE
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for vec2 in dot_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dot_comp

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_comp(cvec,integrand)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> integrand array
    COMPLEX(r8), INTENT(IN) :: integrand(:,:,:,:)

    INTEGER(i4) :: ipol,iv,iy,ix,is,ii
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element into the
!   correct arrays. factors of Jacobian and quadrature weight are already in
!   the test function arrays included in the integrand.
!-------------------------------------------------------------------------------
    iv=1
    IF (ASSOCIATED(cvec%arr)) THEN
      ipol=1
      DO iy=0,cvec%my-1
        DO ix=0,cvec%mx-1
          cvec%arr(:,ix,iy,:)=cvec%arr(:,ix,iy,:)+integrand(:,ipol,1,:)
          cvec%arr(:,ix+1,iy,:)=cvec%arr(:,ix+1,iy,:)+integrand(:,ipol,2,:)
          cvec%arr(:,ix,iy+1,:)=cvec%arr(:,ix,iy+1,:)+integrand(:,ipol,3,:)
          cvec%arr(:,ix+1,iy+1,:)=cvec%arr(:,ix+1,iy+1,:)+integrand(:,ipol,4,:)
          ipol=ipol+1
        ENDDO
      ENDDO
      iv=iv+4
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      DO is=1,cvec%n_side
        ipol=1
        DO iy=0,cvec%my-1
          DO ix=1,cvec%mx
            cvec%arrh(:,is,ix,iy,:)=cvec%arrh(:,is,ix,iy,:)                     &
                                       +integrand(:,ipol,iv,:)
            cvec%arrh(:,is,ix,iy+1,:)=cvec%arrh(:,is,ix,iy+1,:)                 &
                                         +integrand(:,ipol,iv+1,:)
            ipol=ipol+1
          ENDDO
        ENDDO
        iv=iv+2
      ENDDO
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      DO is=1,cvec%n_side
        ipol=1
        DO iy=1,cvec%my
          DO ix=0,cvec%mx-1
            cvec%arrv(:,is,ix,iy,:)=cvec%arrv(:,is,ix,iy,:)                     &
                                       +integrand(:,ipol,iv,:)
            cvec%arrv(:,is,ix+1,iy,:)=cvec%arrv(:,is,ix+1,iy,:)                 &
                                         +integrand(:,ipol,iv+1,:)
            ipol=ipol+1
          ENDDO
        ENDDO
        iv=iv+2
      ENDDO
    ENDIF
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
      DO ii=1,cvec%n_int
        ipol=1
        DO iy=1,cvec%my
          DO ix=1,cvec%mx
            cvec%arri(:,ii,ix,iy,:)=cvec%arri(:,ii,ix,iy,:)                     &
                                       +integrand(:,ipol,iv,:)
            ipol=ipol+1
          ENDDO
        ENDDO
        iv=iv+1
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_comp

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_comp(cvec,component,edge,symm,seam_save)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
    !> save the existing boundary data in seam_save
    LOGICAL, OPTIONAL, INTENT(IN) :: seam_save

    COMPLEX(r8), DIMENSION(cvec%nqty) :: cproj
    COMPLEX(r8), DIMENSION(:,:,:,:,:), POINTER :: arr5d
    REAL(r8), DIMENSION(cvec%nqty,cvec%nqty) :: bcpmat
    INTEGER(i4), DIMENSION(cvec%nqty,2) :: bciarr
    INTEGER(i4) :: iv,ix,iy,imode,ivp,is,isymm
    !> save the existing boundary data in seam_save
    LOGICAL :: seam_save_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
    IF (PRESENT(seam_save)) THEN
      seam_save_loc=seam_save
    ELSE
      seam_save_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,cvec%nqty,bciarr)
!-------------------------------------------------------------------------------
!   the bcdir_set routine combines the bciarr information with local
!   surface-normal and tangential unit directions.  mesh vertices
!   are treated first.
!-------------------------------------------------------------------------------
    vert: DO iv=1,edge%nvert
      IF (.NOT.edge%expoint(iv)) CYCLE
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      CALL bcdir_set(cvec%nqty,bciarr,edge%excorner(iv),isymm,                  &
                     edge%vert_norm(:,iv),edge%vert_tang(:,iv),bcpmat)
      IF (seam_save_loc) THEN
        DO imode=1,cvec%nmodes
          cproj=MATMUL(bcpmat,cvec%arr(:,ix,iy,imode))
          cvec%arr(:,ix,iy,imode)=cvec%arr(:,ix,iy,imode)-cproj
          edge%vert_csave(1:cvec%nqty,imode,iv)=                                &
            edge%vert_csave(1:cvec%nqty,imode,iv)+cproj
        ENDDO
      ELSE
        DO imode=1,cvec%nmodes
          cproj=MATMUL(bcpmat,cvec%arr(:,ix,iy,imode))
          cvec%arr(1:cvec%nqty,ix,iy,imode)=                                    &
            cvec%arr(1:cvec%nqty,ix,iy,imode)-cproj
        ENDDO
      ENDIF
    ENDDO vert
!-------------------------------------------------------------------------------
!   side-centered nodes.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(cvec%arrh)) THEN
      seg: DO iv=1,edge%nvert
        ivp=iv-1
        IF (ivp==0) ivp=edge%nvert
        IF (.NOT.(edge%expoint(iv).AND.edge%expoint(ivp))) CYCLE seg
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          arr5d=>cvec%arrh
        ELSE
          arr5d=>cvec%arrv
        ENDIF
        IF (seam_save_loc) THEN
          DO imode=1,cvec%nmodes
            DO is=1,cvec%n_side
              CALL bcdir_set(cvec%nqty,bciarr,.false.,isymm,                    &
                             edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),     &
                             bcpmat)
              cproj=MATMUL(bcpmat,arr5d(:,is,ix,iy,imode))
              arr5d(:,is,ix,iy,imode)=arr5d(:,is,ix,iy,imode)-cproj
              edge%seg_csave(1:cvec%nqty,is,imode,iv)=                          &
                edge%seg_csave(1:cvec%nqty,is,imode,iv)+cproj
            ENDDO
          ENDDO
        ELSE
          DO imode=1,cvec%nmodes
            DO is=1,cvec%n_side
              CALL bcdir_set(cvec%nqty,bciarr,.false.,isymm,                    &
                             edge%seg_norm(:,is,iv),edge%seg_tang(:,is,iv),     &
                             bcpmat)
              cproj=MATMUL(bcpmat,arr5d(:,is,ix,iy,imode))
              arr5d(:,is,ix,iy,imode)=arr5d(:,is,ix,iy,imode)-cproj
            ENDDO
          ENDDO
        ENDIF
      ENDDO seg
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_comp

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_comp(cvec,edge,nindex,flag)
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> mode number array associated with vector
    INTEGER(i4), INTENT(IN) :: nindex(cvec%nmodes)
    !> flag
    CHARACTER(*), INTENT(IN) :: flag

    INTEGER(i4) :: iv,ivp,ix,iy,imode,imn1,iside,ivec,nvec
    REAL(r8), DIMENSION(cvec%nqty,cvec%nmodes) :: mult
    COMPLEX(r8), DIMENSION(:,:,:,:,:), POINTER :: arr5d
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different Fourier components
!   of a vector.  for a scalar quantity, n>0 are set to 0.  for a
!   vector, thd following components are set to 0
!   n=0:  r and phi
!   n=1:  z
!   n>1:  r, z, and phi
!
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!-------------------------------------------------------------------------------
    mult=0
    imn1=0
    nvec=0
    SELECT CASE(cvec%nqty)
    CASE(1,2,4,5)          !   scalars
      DO imode=1,cvec%nmodes
        IF (nindex(imode)==0) THEN
          mult(:,imode)=1
          EXIT
        ENDIF
      ENDDO
    CASE(3,6,9,12,15,18)   !   3-vectors
      nvec=cvec%nqty/3
      DO ivec=0,nvec-1
        DO imode=1,cvec%nmodes
          IF (nindex(imode)==0) THEN
            mult(3*ivec+2,imode)=1
            CYCLE
          ENDIF
          IF (nindex(imode)==1) THEN
            mult(3*ivec+1,imode)=1
            IF (flag=='cyl_vec') imn1=imode
            IF (flag=='init_cyl_vec') mult(3*ivec+3,imode)=1
          ENDIF
        ENDDO
      ENDDO
    CASE DEFAULT
      CALL par%nim_stop("vec_rect_2D::regularity_cvec"//                        &
                    " inconsistent # of components.")
    END SELECT
!-------------------------------------------------------------------------------
!   loop over block border elements and apply mult to the vertices
!   at R=0.  also combine rhs for n=1 r and phi comps.
!-------------------------------------------------------------------------------
    vert: DO iv=1,edge%nvert
      IF (.NOT.edge%r0point(iv)) CYCLE
      ix=edge%vertex(iv)%intxy(1)
      iy=edge%vertex(iv)%intxy(2)
      IF (imn1/=0) THEN
        DO ivec=0,nvec-1
          cvec%arr(3*ivec+1,ix,iy,imn1)=cvec%arr(3*ivec+1,ix,iy,imn1)           &
                                        -(0,1)*cvec%arr(3*ivec+3,ix,iy,imn1)
        ENDDO
      ENDIF
      DO imode=1,cvec%nmodes
        cvec%arr(:,ix,iy,imode)=cvec%arr(:,ix,iy,imode)*mult(:,imode)
      ENDDO
      IF (ASSOCIATED(cvec%arrh)) THEN
        ivp=iv-1
        IF (ivp==0) ivp=edge%nvert
        IF (.NOT.edge%r0point(ivp)) CYCLE
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          arr5d=>cvec%arrh
        ELSE
          arr5d=>cvec%arrv
        ENDIF
        IF (imn1/=0) THEN
          DO ivec=0,nvec-1
            arr5d(3*ivec+1,:,ix,iy,imn1)=arr5d(3*ivec+1,:,ix,iy,imn1)           &
                                         -(0,1)*arr5d(3*ivec+3,:,ix,iy,imn1)
          ENDDO
        ENDIF
        DO imode=1,cvec%nmodes
          DO iside=1,cvec%n_side
            arr5d(:,iside,ix,iy,imode)=arr5d(:,iside,ix,iy,imode)*mult(:,imode)
          ENDDO
        ENDDO
      ENDIF
    ENDDO vert
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_comp

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_comp(cvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_comp',iftn,idepth)
    DO iv=1,cvec%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cvec%mx+1,cvec%mx+cvec%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cvec%mx+cvec%my+1,2*cvec%mx+cvec%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*cvec%mx+cvec%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_comp

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_comp(cvec,edge,nqty,n_side,ifs,ife,do_avg)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> first Fourier mode to load (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
    !> last Fourier mode to load (default nmodes)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ife
    !> apply edge average if true
    LOGICAL, OPTIONAL, INTENT(IN) :: do_avg

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns,nm,im,ims,ime
    LOGICAL :: do_avg_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cvec%n_side
    ENDIF
    IF (PRESENT(ifs)) THEN
      ims=ifs
    ELSE
      ims=1_i4
    ENDIF
    IF (PRESENT(ife)) THEN
      ime=ife
    ELSE
      ime=cvec%nmodes
    ENDIF
    nm=ime-ims+1
    IF (PRESENT(do_avg)) THEN
      do_avg_loc=do_avg
    ELSE
      do_avg_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%imode_start=ims
    edge%nmodes_loaded=nm
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      iq=1
      DO im=ims,ime
        edge%vert_cin(iq:nq+iq-1,iv)=                                           &
          cvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2),im)
        iq=iq+nq
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  load side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            DO im=ims,ime
              edge%seg_cin(iq:nq+iq-1,iv)=cvec%arrh(1:nq,is,ix,iy,im)
              iq=iq+nq
            ENDDO
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            DO im=ims,ime
              edge%seg_cin(iq:nq+iq-1,iv)=cvec%arrv(1:nq,is,ix,iy,im)
              iq=iq+nq
            ENDDO
          ENDDO
        ENDIF
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   apply average factor
!-------------------------------------------------------------------------------
    IF (do_avg_loc) THEN
      DO iv=1,edge%nvert
        edge%vert_cin(1:nq*nm,iv)=                                              &
          edge%vert_cin(1:nq*nm,iv)*edge%vertex(iv)%ave_factor
        IF (ns/=0) THEN
          edge%seg_cin(1:nq*nm*ns,iv)=                                          &
            edge%seg_cin(1:nq*nm*ns,iv)*edge%segment(iv)%ave_factor
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_comp

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_comp(cvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,iq,is,ist,ise,nq,ns,im,ims,ime
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
    ims=edge%imode_start
    ime=edge%nmodes_loaded+ims-1_i4
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      iq=1
      DO im=ims,ime
        cvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2),im)=    &
          edge%vert_cout(iq:nq+iq-1,iv)
        iq=iq+nq
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.  unload side-centered nodes in the
!   direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        iq=1
        CALL edge%load_limits(edge%segment(iv)%load_dir,ns,ist,ise)
        IF (edge%segment(iv)%h_side) THEN
          DO is=ist,ise,edge%segment(iv)%load_dir
            DO im=ims,ime
              cvec%arrh(1:nq,is,ix,iy,im)=edge%seg_cout(iq:nq+iq-1,iv)
              iq=iq+nq
            ENDDO
          ENDDO
        ELSE
          DO is=ist,ise,edge%segment(iv)%load_dir
            DO im=ims,ime
              cvec%arrv(1:nq,is,ix,iy,im)=edge%seg_cout(iq:nq+iq-1,iv)
              iq=iq+nq
            ENDDO
          ENDDO
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_comp

!-------------------------------------------------------------------------------
!* load a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_save_comp(cvec,edge,nqty,n_side,ifs,ife)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> first Fourier mode to load (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
    !> last Fourier mode to load (default nmodes)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ife

    INTEGER(i4) :: ix,iy,iv,nq,ns,ims,ime
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_save_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cvec%n_side
    ENDIF
    IF (PRESENT(ifs)) THEN
      ims=ifs
    ELSE
      ims=1_i4
    ENDIF
    IF (PRESENT(ife)) THEN
      ime=ife
    ELSE
      ime=cvec%nmodes
    ENDIF
!-------------------------------------------------------------------------------
!   copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vert_csave(1:nq,ims:ime,iv)=                                         &
        cvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2),ims:ime)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          edge%seg_csave(1:nq,:,ims:ime,iv)=cvec%arrh(1:nq,:,ix,iy,ims:ime)
        ELSE
          edge%seg_csave(1:nq,:,ims:ime,iv)=cvec%arrv(1:nq,:,ix,iy,ims:ime)
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_save_comp

!-------------------------------------------------------------------------------
!* unload a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_add_save_comp(cvec,edge,nqty,n_side,ifs,ife)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be add to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> edge to add from
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> first Fourier mode to load (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
    !> last Fourier mode to load (default nmodes)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: ife

    INTEGER(i4) :: ix,iy,iv,nq,ns,ims,ime
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_add_save_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cvec%n_side
    ENDIF
    IF (PRESENT(ifs)) THEN
      ims=ifs
    ELSE
      ims=1_i4
    ENDIF
    IF (PRESENT(ife)) THEN
      ime=ife
    ELSE
      ime=cvec%nmodes
    ENDIF
!-------------------------------------------------------------------------------
!   copy from edge storage to block-internal data. vertex-centered data first.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      cvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2),ims:ime)= &
        cvec%arr(1:nq,edge%vertex(iv)%intxy(1),edge%vertex(iv)%intxy(2),ims:ime)&
        +edge%vert_csave(1:nq,ims:ime,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   element side-centered data.
!-------------------------------------------------------------------------------
    IF (ns/=0) THEN
      DO iv=1,edge%nvert
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        IF (edge%segment(iv)%h_side) THEN
          cvec%arrh(1:nq,:,ix,iy,ims:ime)=cvec%arrh(1:nq,:,ix,iy,ims:ime)+      &
            edge%seg_csave(1:nq,:,ims:ime,iv)
        ELSE
          cvec%arrv(1:nq,:,ix,iy,ims:ime)=cvec%arrv(1:nq,:,ix,iy,ims:ime)+      &
            edge%seg_csave(1:nq,:,ims:ime,iv)
        ENDIF
      ENDDO
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_add_save_comp

!-------------------------------------------------------------------------------
!> Assume vector data represents the diagonal of a matrix and apply a matrix
!  multiply.
!-------------------------------------------------------------------------------
  SUBROUTINE apply_diag_matvec_comp(cvec,input_vec,output_vec)
    IMPLICIT NONE

    !> vector that represents a diagonal matrix
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> vector to multiply
    CLASS(cvector), INTENT(IN) :: input_vec
    !> output vector = cvec (matrix diagonal) dot input_vec
    CLASS(cvector), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_diag_matvec_comp',iftn,idepth)
    SELECT TYPE (input_vec)
    TYPE IS (vec_rect_2D_comp)
      SELECT TYPE (output_vec)
      TYPE IS (vec_rect_2D_comp)
        IF (ASSOCIATED(cvec%arr)) THEN
          output_vec%arr=cvec%arr*input_vec%arr
        ENDIF
        IF (ASSOCIATED(cvec%arrh)) THEN
          output_vec%arrh=cvec%arrh*input_vec%arrh
        ENDIF
        IF (ASSOCIATED(cvec%arrv)) THEN
          output_vec%arrv=cvec%arrv*input_vec%arrv
        ENDIF
        IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
          output_vec%arri=cvec%arri*input_vec%arri
        ENDIF
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_comp for output_vec'            &
                          //' in apply_diag_matvec_comp')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for input_vec'               &
                        //' in apply_diag_matvec_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_diag_matvec_comp

!-------------------------------------------------------------------------------
!> Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
  SUBROUTINE invert_comp(cvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'invert_comp',iftn,idepth)
    IF (ASSOCIATED(cvec%arr)) THEN
      cvec%arr=1._r8/cvec%arr
    ENDIF
    IF (ASSOCIATED(cvec%arrh)) THEN
      cvec%arrh=1._r8/cvec%arrh
    ENDIF
    IF (ASSOCIATED(cvec%arrv)) THEN
      cvec%arrv=1._r8/cvec%arrv
    ENDIF
    IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
      cvec%arri=1._r8/cvec%arri
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE invert_comp

!-------------------------------------------------------------------------------
!* Assign to the vector for testing
!-------------------------------------------------------------------------------
  SUBROUTINE assign_for_testing_comp(cvec,operation,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_comp), INTENT(INOUT) :: cvec
    !> operation: 'constant' or 'index'
    CHARACTER(*), INTENT(IN) :: operation
    !> optional constant value for operation='constant'
    COMPLEX(r8), INTENT(IN), OPTIONAL :: const

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    COMPLEX(r8) :: fac
    INTEGER(i4) :: ii1,ii2,ii3,ii4,ii5,ind

    CALL timer%start_timer_l2(mod_name,'assign_for_testing_comp',iftn,idepth)
    fac=1._r8
    IF (PRESENT(const)) fac=const
    SELECT CASE(operation)
    CASE("constant")
      IF (ASSOCIATED(cvec%arr)) THEN
        ASSOCIATE (arr=>cvec%arr)
          arr=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        ASSOCIATE (arrh=>cvec%arrh)
          arrh=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        ASSOCIATE (arrv=>cvec%arrv)
          arrv=fac
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>cvec%arri)
          arri=fac
        END ASSOCIATE
      ENDIF
    CASE("index")
      IF (ASSOCIATED(cvec%arr)) THEN
        ASSOCIATE (arr=>cvec%arr)
          DO ii1=LBOUND(arr,1),UBOUND(arr,1)
            DO ii2=LBOUND(arr,2),UBOUND(arr,2)
              DO ii3=LBOUND(arr,3),UBOUND(arr,3)
                DO ii4=LBOUND(arr,4),UBOUND(arr,4)
                  ind=ii1*SIZE(arr,2)*SIZE(arr,3)*SIZE(arr,4)+                  &
                      ii2*SIZE(arr,3)*SIZE(arr,4)+ii3*SIZE(arr,4)+ii4
                  arr(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        ASSOCIATE (arrh=>cvec%arrh)
          DO ii1=LBOUND(arrh,1),UBOUND(arrh,1)
            DO ii2=LBOUND(arrh,2),UBOUND(arrh,2)
              DO ii3=LBOUND(arrh,3),UBOUND(arrh,3)
                DO ii4=LBOUND(arrh,4),UBOUND(arrh,4)
                  DO ii5=LBOUND(arrh,5),UBOUND(arrh,5)
                    ind=ii1*SIZE(arrh,2)*SIZE(arrh,3)*SIZE(arrh,4)*SIZE(arrh,5)+&
                        ii2*SIZE(arrh,3)*SIZE(arrh,4)*SIZE(arrh,5)+             &
                        ii3*SIZE(arrh,4)*SIZE(arrh,5)+ii4*SIZE(arrh,5)+ii5
                    arrh(ii1,ii2,ii3,ii4,ii5)=ind
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        ASSOCIATE (arrv=>cvec%arrv)
          DO ii1=LBOUND(arrv,1),UBOUND(arrv,1)
            DO ii2=LBOUND(arrv,2),UBOUND(arrv,2)
              DO ii3=LBOUND(arrv,3),UBOUND(arrv,3)
                DO ii4=LBOUND(arrv,4),UBOUND(arrv,4)
                  DO ii5=LBOUND(arrv,5),UBOUND(arrv,5)
                    ind=ii1*SIZE(arrv,2)*SIZE(arrv,3)*SIZE(arrv,4)*SIZE(arrv,5)+&
                        ii2*SIZE(arrv,3)*SIZE(arrv,4)*SIZE(arrv,5)+             &
                        ii3*SIZE(arrv,4)*SIZE(arrv,5)+ii4*SIZE(arrv,5)+ii5
                    arrv(ii1,ii2,ii3,ii4,ii5)=ind
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>cvec%arri)
          DO ii1=LBOUND(arri,1),UBOUND(arri,1)
            DO ii2=LBOUND(arri,2),UBOUND(arri,2)
              DO ii3=LBOUND(arri,3),UBOUND(arri,3)
                DO ii4=LBOUND(arri,4),UBOUND(arri,4)
                  DO ii5=LBOUND(arri,5),UBOUND(arri,5)
                    ind=ii1*SIZE(arri,2)*SIZE(arri,3)*SIZE(arri,4)*SIZE(arri,5)+&
                        ii2*SIZE(arri,3)*SIZE(arri,4)*SIZE(arri,5)+             &
                        ii3*SIZE(arri,4)*SIZE(arri,5)+ii4*SIZE(arri,5)+ii5
                    arri(ii1,ii2,ii3,ii4,ii5)=ind
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        END ASSOCIATE
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop('Expected operation=constant or index'                  &
                        //' in assign_for_testing_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_for_testing_comp

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
  LOGICAL FUNCTION test_if_equal_comp(cvec,cvec2) RESULT(output)
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_comp), INTENT(IN) :: cvec
    !> vector to test against
    CLASS(cvector), INTENT(IN) :: cvec2

    INTEGER(i4) :: shape4a(4),shape4b(4),shape5a(5),shape5b(5)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'test_if_equal_comp',iftn,idepth)
    output=.TRUE.
    SELECT TYPE (cvec2)
    TYPE IS (vec_rect_2D_comp)
      IF (ASSOCIATED(cvec%arr)) THEN
        shape4a=SHAPE(cvec%arr)
        shape4b=SHAPE(cvec2%arr)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arr == cvec2%arr)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arr)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cvec%arrh)) THEN
        shape5a=SHAPE(cvec%arrh)
        shape5b=SHAPE(cvec2%arrh)
        IF (.NOT.ALL(shape5a == shape5b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arrh == cvec2%arrh)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arrh)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cvec%arrv)) THEN
        shape5a=SHAPE(cvec%arrv)
        shape5b=SHAPE(cvec2%arrv)
        IF (.NOT.ALL(shape5a == shape5b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arrv == cvec2%arrv)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arrv)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cvec%arri).AND..NOT.cvec%skip_elim_interior) THEN
        shape5a=SHAPE(cvec%arri)
        shape5b=SHAPE(cvec2%arri)
        IF (.NOT.ALL(shape5a == shape5b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cvec%arri == cvec2%arri)) output=.FALSE.
      ELSE IF (ASSOCIATED(cvec2%arri)) THEN
        output=.FALSE.
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec2 in'//              &
                        ' test_if_equal_comp')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION test_if_equal_comp
