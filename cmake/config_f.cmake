######################################################################
#
# Create a Fortran preprocessor include file, strip comments 
# from config.h
#
######################################################################

add_custom_command(
  OUTPUT config.f
  COMMAND sed;'/^\\//;d';<;config.h;>;config.f
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)
add_custom_target(config_f DEPENDS config.f)
set_directory_properties(PROPERTY ADDITIONAL_MAKE_CLEAN_FILES config.f)
install(FILES ${CMAKE_BINARY_DIR}/config.f
              ${CMAKE_BINARY_DIR}/config.h
    DESTINATION share/
    PERMISSIONS OWNER_READ OWNER_WRITE
                GROUP_READ GROUP_WRITE
                WORLD_READ
)
