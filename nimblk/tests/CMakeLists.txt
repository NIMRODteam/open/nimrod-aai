######################################################################
#
# CMakeLists.txt for nimblk/tests
#
######################################################################

# Make sure we run locally
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

set(NIMBLKTEST_SOURCES
    block_registry.f
    test_fem_linalg_xfer_real.f
    test_fem_linalg_xfer_comp.f
    test_fem_linalg_xfer_cv1m.f
)

add_library(nimblktest ${NIMBLKTEST_SOURCES})
target_link_libraries(nimblktest PUBLIC nimblk nimfem nimfemtest nimutest)
target_include_directories(nimblktest PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)

addNimUnitTest(SOURCEFILES test_block.f
               TESTARGS alloc_dealloc set h5io
	       TEST_TYPE rblock
               RMFILES *.h5;
               LINK_LIBS nimblktest nimfemtest
               LABELS rblock)

if (MPI_FOUND)
  if (MPI_TEST_NPROCS GREATER_EQUAL 4)
    set(nlayers 1 2 4)
  elseif (MPI_TEST_NPROCS GREATER_EQUAL 2)
    set(nlayers 1 2)
  else ()
    set(nlayers 1)
  endif ()
else ()
  set(nlayers 1)
endif ()

addNimUnitTest(EXEC test_block
               TESTARGS dump
	       TEST_TYPE rblock
	       NLAYERS ${nlayers}
	       NMODES 4 6
               RMFILES *.h5;
               LINK_LIBS nimblktest nimfemtest
               LABELS rblock
	       USE_MPI)

addNimUnitTest(SOURCEFILES test_fem_linalg_xfer.f
               TESTARGS alloc_xfer alloc_mat
	       TEST_TYPE h1_rect_2D_real h1_rect_2D_real_acc h1_rect_2D_comp h1_rect_2D_comp_acc
               RMFILES *.h5;
               LINK_LIBS nimblktest nimfemtest
               LABELS rblock)
