!-------------------------------------------------------------------------------
!! Defines integrand abstract interfaces
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Defines integrand abstract interfaces
!-------------------------------------------------------------------------------
MODULE integrand_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: integrand_vec_real,integrand_vec_comp,                              &
            integrand_mat_real,integrand_mat_comp

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  compute a vector integrand per element
!-------------------------------------------------------------------------------
    SUBROUTINE integrand_vec_real(bl,integrand)
      USE local
      USE gblock_mod
      !> block associated with integrand
      CLASS(gblock), INTENT(IN) :: bl
      !> integrand to be computed
      REAL(r8), CONTIGUOUS, INTENT(OUT) :: integrand(:,:,:)
    END SUBROUTINE integrand_vec_real

!-------------------------------------------------------------------------------
!*  compute a vector integrand per element
!-------------------------------------------------------------------------------
    SUBROUTINE integrand_vec_comp(bl,integrand)
      USE local
      USE gblock_mod
      !> block associated with integrand
      CLASS(gblock), INTENT(IN) :: bl
      !> integrand to be computed
      COMPLEX(r8), CONTIGUOUS, INTENT(OUT) :: integrand(:,:,:,:)
    END SUBROUTINE integrand_vec_comp

!-------------------------------------------------------------------------------
!*  compute a matrix integrand per element
!-------------------------------------------------------------------------------
    SUBROUTINE integrand_mat_real(bl,integrand)
      USE local
      USE gblock_mod
      !> block associated with integrand
      CLASS(gblock), INTENT(IN) :: bl
      !> integrand to be computed
      REAL(r8), CONTIGUOUS, INTENT(OUT) :: integrand(:,:,:,:,:)
    END SUBROUTINE integrand_mat_real

!-------------------------------------------------------------------------------
!*  compute a matrix integrand per element
!-------------------------------------------------------------------------------
    SUBROUTINE integrand_mat_comp(bl,integrand,imode,jmode)
      USE local
      USE gblock_mod
      !> block associated with integrand
      CLASS(gblock), INTENT(IN) :: bl
      !> integrand to be computed
      COMPLEX(r8), CONTIGUOUS, INTENT(OUT) :: integrand(:,:,:,:,:)
      !> mode number of matrix column
      INTEGER(i4), INTENT(IN) :: imode
      !> mode number of matrix row
      INTEGER(i4), INTENT(IN) :: jmode
    END SUBROUTINE integrand_mat_comp
  END INTERFACE

END MODULE integrand_mod
