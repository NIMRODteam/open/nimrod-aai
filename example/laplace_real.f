!-------------------------------------------------------------------------------
!! run a time-dependent calculation of a Laplacian
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* run a time-dependent calculation of a Laplacian
!-------------------------------------------------------------------------------
PROGRAM laplace
  USE alloc_linalg_mod
  USE dump_mod
  USE finite_element_mod
  USE fourier_input_mod
  USE gblock_mod
  USE grid_input_mod
  USE integrand_mod
  USE io
  USE iter_all_mod
  USE iterdata_mod
  USE laplace_input
  USE linalg_utils_mod
  USE local
  USE nodal_mod
  USE matrix_mod
#ifdef OBJ_MEM_PROF
  USE memlog
#endif
  USE pardata_mod
  USE preconditioner_mod
  USE quadrature_mod
  USE rblock_mod ! remove after issue #122 is addressed
  USE seam_mod
  USE simple_seam_mod ! remove after issue #122 is addressed
  USE timer_mod
  USE vector_mod
  USE xfer_vector_to_fem_mod
  IMPLICIT NONE

  CHARACTER(128) :: input_file='example.in'
  CHARACTER(512) :: msg
  LOGICAL :: dir_bc
  INTEGER(i4) :: ii,nargs,istep0,istep,ibl
  REAL(r8) :: time,t0,err,ksq
  TYPE(block_storage), ALLOCATABLE :: blk(:)
  TYPE(seam_type) :: seam
  TYPE(rfem_storage), ALLOCATABLE, TARGET :: field(:)
  TYPE(qp_real), ALLOCATABLE :: qpfield(:)
  TYPE(field_list_pointer) :: io_field_list(1)
  TYPE(rvec_storage), ALLOCATABLE :: rhs(:),sln(:)
  TYPE(rmat_storage), ALLOCATABLE :: mat(:)
  CLASS(precon_real), ALLOCATABLE :: precon
  CHARACTER(1), ALLOCATABLE :: vcomp(:)
  REAL(r8), ALLOCATABLE :: sln_norm(:,:)
  TYPE(iterdata) :: itdat
  INTEGER(i4) :: idepth,idepth_loop
  INTEGER(i4), SAVE :: iftn=-1,iftn_loop=-1

  ofname='laplace_real.out'
  CALL par%init
!-------------------------------------------------------------------------------
! determine configuration from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs==1) THEN
    CALL get_command_argument(1,input_file)
  ENDIF
  IF (input_file=='-h'.OR.nargs>1) THEN
    CALL print_usage
  ENDIF
!-------------------------------------------------------------------------------
! read input
!-------------------------------------------------------------------------------
  IF (par%node==0) CALL laplace_read_namelist(input_file)
  CALL laplace_bcast_input
  dir_bc=dirichlet_boundary_condition
!-------------------------------------------------------------------------------
! initializations
!-------------------------------------------------------------------------------
  CALL fch5_init
  CALL timer%init(directive_wait=timer_directive_wait)
  CALL timer%start_timer_l0('laplace','laplace',iftn,idepth)
!-------------------------------------------------------------------------------
! read dump file which initializes the parallel decomposition
!-------------------------------------------------------------------------------
  CALL par%nim_write('Reading dump file.')
  io_field_list(1)%name="field"
  io_field_list(1)%type="rfem_storage"
  CALL dump_read(TRIM(dump_file),t0,istep0,blk,seam,io_field_list,              &
                 all_blks_on_gpu=on_gpu)
  time=t0
!-------------------------------------------------------------------------------
! the keff array contains ksq, extract and reset it.
!-------------------------------------------------------------------------------
  ksq=par%keff_total(1)
  par%keff_total=par%nindex_total*twopi
  IF (.NOT.grid_in%torgeom) par%keff_total=par%keff_total/grid_in%per_length
  par%keff=par%keff_total(par%mode_lo:par%mode_hi)
!-------------------------------------------------------------------------------
! set up blocks
!-------------------------------------------------------------------------------
  CALL par%nim_write('Initializing.')
  DO ibl=1,par%nbl
    CALL blk(ibl)%b%block_intg_formula_set(ngr,int_formula,int_formula)
    CALL blk(ibl)%b%block_metric_set(grid_in%torgeom)
  ENDDO
!-------------------------------------------------------------------------------
! copy out field list and reset as pointer
!-------------------------------------------------------------------------------
  ALLOCATE(field(par%nbl))
  DO ibl=1,par%nbl
    SELECT TYPE(iofield=>io_field_list(1)%p(ibl))
    TYPE IS (rfem_storage)
      CALL iofield%f%move_field_to(field(ibl)%f)
      DEALLOCATE(iofield%f)
    END SELECT
  ENDDO
  DEALLOCATE(io_field_list(1)%p)
  io_field_list(1)%p=>field
!-------------------------------------------------------------------------------
! initialize seams
!-------------------------------------------------------------------------------
  CALL seam%init(field(1)%f%nqty,field(1)%f%nqty,field(1)%f%pd-1_i4,1_i4)
!-------------------------------------------------------------------------------
! set up linear algebra and quadrature point structures
!-------------------------------------------------------------------------------
  ALLOCATE(rhs(par%nbl),sln(par%nbl),mat(par%nbl),qpfield(par%nbl))
  ALLOCATE(vcomp(field(1)%f%nqty))
  vcomp='s'
  DO ibl=1,par%nbl
    CALL alloc_vector_for_fem(rhs(ibl)%v,field(ibl)%f)
    CALL rhs(ibl)%v%alloc_with_mold(sln(ibl)%v)
    CALL sln(ibl)%v%zero
    CALL rhs(ibl)%v%set_edge_vars(seam%s(ibl))
    SELECT TYPE(bl=>blk(ibl)%b) ! remove after issue #122 is addressed
    TYPE IS (rblock)
      CALL set_edge_norm_tang_rect(seam%s(ibl),bl%mx,bl%my,bl%poly_degree-1_i4)
    END SELECT
    CALL alloc_matrix_for_fem(mat(ibl)%m,field(ibl)%f,vcomp)
    CALL field(ibl)%f%qp_alloc(qpfield(ibl),blk(ibl)%b%ng)
    CALL field(ibl)%f%init_basis_ftn(blk(ibl)%b%xg)
    IF (.NOT.compute_integrand_alpha) THEN
      CALL field(ibl)%f%init_block_basis_ftn(blk(ibl)%b%metric,blk(ibl)%b%ng)
    ENDIF
  ENDDO
  DEALLOCATE(vcomp)
!-------------------------------------------------------------------------------
! set up the preconditioner
!-------------------------------------------------------------------------------
  CALL precon_init(precon,precon_type,rhs)
!-------------------------------------------------------------------------------
! set iterdata input
!-------------------------------------------------------------------------------
  itdat%maxit=maxit
  itdat%tol=tol
!-------------------------------------------------------------------------------
! report memory usage
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
  CALL memlogger%report(1,1,'before loop')
#endif
!-------------------------------------------------------------------------------
! time-step loop to solve the diffusion equation
! while we could form this linear operator once outside the loop, do it inside
! to better mirror the timing behavior with a nonlinear operator
!-------------------------------------------------------------------------------
  ALLOCATE(sln_norm(3,nstep))
  CALL par%nim_write('Running time-step loop.')
  DO istep=istep0+1,istep0+nstep
!-------------------------------------------------------------------------------
!   ignore the first step for timing purposes to avoid initialization costs
!-------------------------------------------------------------------------------
    IF (istep==istep0+2) THEN
      CALL timer%start_timer_l0('laplace','loop+1',iftn_loop,idepth_loop)
      IF (reset_timers_at_step2) THEN
        timer%time_by_function_inclusive=0
        timer%time_by_function_exclusive=0
        timer%calls_by_function=0
      ENDIF
    ENDIF
    DO ibl=1,par%nbl
      CALL field(ibl)%f%qp_update(qpfield(ibl),.TRUE.,blk(ibl)%b%metric)
    ENDDO
    IF (static_condensation) THEN
      CALL create_matrix(blk,mat,precon,seam,diff_op,dir_bc,'all',.TRUE.)
      CALL create_vector(blk,rhs,seam,diff_rhs,dir_bc,'all',elim_matrix=mat)
      DO ibl=1,par%nbl
        CALL sln(ibl)%v%zero
        sln(ibl)%v%skip_elim_interior=.TRUE.
      ENDDO
    ELSE
      CALL create_matrix(blk,mat,precon,seam,diff_op,dir_bc,'all',.FALSE.)
      CALL create_vector(blk,rhs,seam,diff_rhs,dir_bc,'all')
    ENDIF
    CALL iter_cg_real_dir_solve(mat,precon,rhs,sln,seam,itdat)
    DO ibl=1,par%nbl
      IF (static_condensation.AND.itdat%converged) THEN
        CALL mat(ibl)%m%elim_postsolve(rhs(ibl)%v,sln(ibl)%v)
        rhs(ibl)%v%skip_elim_interior=.FALSE.
      ENDIF
      CALL xfer_vector_to_fem(sln(ibl)%v,field(ibl)%f)
    ENDDO
    IF (itdat%converged) THEN
      WRITE(msg,'(a,i6,a,es10.3,a,i6,a)')                                       &
        'Step=',istep,': solved system to err=',itdat%err,' in ',               &
        itdat%its,' iterations'
      CALL par%nim_write(msg)
      WRITE(msg,'(a,es10.3,2a)')                                                &
        '  rhs_norm=',itdat%rhs_norm,' seed=',TRIM(itdat%seed)
      CALL par%nim_write(msg)
!-------------------------------------------------------------------------------
!     calculate the time-discretized decay after the first step. This does not
!     account for the norm of the initial state as there's no good way to
!     assess that.
!-------------------------------------------------------------------------------
      ii=istep-istep0
      sln_norm(1,ii)=SQRT(norm(sln,seam,'l2'))
      sln_norm(2,ii)=sln_norm(1,1)                                              &
                            *((1._r8-dt*(1._r8-theta)*diff*ksq)                 &
                              /(1._r8+dt*theta*diff*ksq))**(ii-1)
      sln_norm(3,ii)=sln_norm(1,1)*EXP(-(time-t0)*diff*ksq)
    ELSE
!-------------------------------------------------------------------------------
!     Solver failed to converge. Output a dump file with the residual
!     in the field.
!-------------------------------------------------------------------------------
      WRITE(msg,'(a,i6,a,es10.3,a,i6,a)')                                       &
        'Step=',istep,': failed to converge with err=',itdat%err,' in ',        &
        itdat%its,' iterations'
      CALL par%nim_write(msg)
      WRITE(msg,'(a,es10.3,2a)')                                                &
        '  rhs_norm=',itdat%rhs_norm,' seed=',TRIM(itdat%seed)
      CALL par%nim_write(msg)
      CALL par%nim_write('Writing dump.resid.h5 with residual error in field')
      CALL dump_write(time,istep,blk,seam,io_field_list,'dump.resid.h5')
      CALL par%all_barrier
      CALL par%nim_stop('not ok, convergence failure.',clean_shutdown=.TRUE.)
    ENDIF
    time=time+dt
  ENDDO
  IF (istep>=istep0+2) CALL timer%end_timer_l0(iftn_loop,idepth_loop)
!-------------------------------------------------------------------------------
! write dump file and exit
!-------------------------------------------------------------------------------
  CALL par%nim_write('Writing final dump file.')
  IF (itdat%converged) THEN
    CALL dump_write(time,istep0+nstep,blk,seam,io_field_list)
  ENDIF
!-------------------------------------------------------------------------------
! deallocate
!-------------------------------------------------------------------------------
  CALL seam%dealloc
  CALL precon%dealloc
  DO ibl=1,par%nbl
    CALL rhs(ibl)%v%dealloc
    CALL sln(ibl)%v%dealloc
    CALL mat(ibl)%m%dealloc
    CALL field(ibl)%f%qp_dealloc(qpfield(ibl))
    CALL field(ibl)%f%dealloc
    CALL blk(ibl)%b%dealloc
  ENDDO
  DEALLOCATE(rhs,sln,mat,qpfield,field,blk,precon)
  CALL timer%end_timer_l0(iftn,idepth)
!-------------------------------------------------------------------------------
! display timings and time-discretized expected result
!-------------------------------------------------------------------------------
  IF (par%node==0) CALL timer%report
  IF (par%node==0.AND.itdat%converged) THEN
    CALL par%nim_write('  sln_norm          expected          exact')
    DO istep=1,nstep
      WRITE(msg,'(3es18.9)') sln_norm(:,istep)
      CALL par%nim_write(msg)
    ENDDO
    err=ABS((sln_norm(1,nstep)-ABS(sln_norm(2,nstep)))/sln_norm(2,nstep))
    WRITE(msg,'(a,es10.3)') 'normalized error = ',err
    CALL par%nim_write(msg)
    IF (err < 1.e4_r8*tol*nstep) THEN
      CALL par%nim_write('ok - calculation agrees with expectation')
    ELSE
      CALL par%nim_write('not ok - calculation does not agree with expectation')
    ENDIF
  ENDIF
  DEALLOCATE(sln_norm)
  CALL timer%finalize
  CALL fch5_dealloc
#ifdef OBJ_MEM_PROF
  CALL memlogger%finalize
#endif
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Solve the time-dependent diffusion equation')
    CALL par%nim_write('  df/dt = div diff grad f')
    CALL par%nim_write('  and check time-discretized self-similar decay rates')
    CALL par%nim_write('Usage: ./laplace_real <input namelist file>')
    CALL par%nim_write(                                                         &
           'Default input namelist file name is example.in if not specified')
    CALL par%nim_write(                                                         &
           'Warning: there are no checks for incompatible/incorrect options')
    CALL par%nim_stop('check input',clean_shutdown=.TRUE.)
  END SUBROUTINE print_usage

!-------------------------------------------------------------------------------
! Set up the preconditioner
!-------------------------------------------------------------------------------
  SUBROUTINE precon_init(precon,precon_type,rhs)
    USE precon_jacobi_real_mod
    USE precon_none_real_mod
    IMPLICIT NONE

    TYPE(rvec_storage), ALLOCATABLE, INTENT(IN) :: rhs(:)
    CHARACTER(64), INTENT(IN) :: precon_type
    CLASS(precon_real), ALLOCATABLE, INTENT(INOUT) :: precon

    SELECT CASE (precon_type)
    CASE ('none')
      ALLOCATE(precon_none_real::precon)
      SELECT TYPE (precon)
      CLASS IS (precon_none_real)
        CALL precon%alloc
      END SELECT
    CASE ('jacobi')
      ALLOCATE(precon_jacobi_real::precon)
      SELECT TYPE (precon)
      CLASS IS (precon_jacobi_real)
        CALL precon%alloc(rhs)
      END SELECT
    CASE DEFAULT
      CALL par%nim_stop('preconditioner not recognized',clean_shutdown=.TRUE.)
    END SELECT
  END SUBROUTINE precon_init

!-------------------------------------------------------------------------------
! Time-dependent diffusion operator integrand routine
!-------------------------------------------------------------------------------
  SUBROUTINE diff_op(bl,integrand)
    USE local
    USE gblock_mod
    IMPLICIT NONE

    !> block associated with integrand
    CLASS(gblock), INTENT(IN) :: bl
    !> integrand to be computed
    REAL(r8), CONTIGUOUS, INTENT(OUT) :: integrand(:,:,:,:,:)

    INTEGER(i4) :: iq,iv,jv,ie,ig
    REAL(r8) :: tmpsum,fac
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1('diff_op','diff_op',iftn,idepth)
    fac=dt*diff*theta
    IF (compute_integrand_alpha) THEN
!-------------------------------------------------------------------------------
!     this version computes the test/trial functions on the fly
!-------------------------------------------------------------------------------
      ASSOCIATE (f=>field(bl%ibl)%f,alpha=>field(bl%ibl)%f%qa%alf,              &
                 aldf=>field(bl%ibl)%f%qa%aldf,wdetj=>bl%metric%wdetj,          &
                 ijac=>bl%metric%ijac)
        !$acc parallel present(integrand,alpha,aldf,wdetj,ijac,bl,f)            &
        !$acc copyin(fac) async(bl%id) if(bl%on_gpu)
        !$acc loop gang worker
        DO ie=1,bl%nel
          !$acc loop vector collapse(2) independent private(tmpsum)
          DO iv=1,f%nbasis
            DO jv=1,f%nbasis
              tmpsum=0._r8
              !$acc loop seq
              DO ig=1,bl%ng
                tmpsum=tmpsum                                                   &
                       +wdetj(ig,ie)*(alpha(ig,jv)*alpha(ig,iv)                 &
                                      +fac*((ijac(ig,ie,1,1)*aldf(ig,jv,1)      &
                                            +ijac(ig,ie,1,2)*aldf(ig,jv,2))     &
                                           *(ijac(ig,ie,1,1)*aldf(ig,iv,1)      &
                                            +ijac(ig,ie,1,2)*aldf(ig,iv,2))     &
                                           +(ijac(ig,ie,2,1)*aldf(ig,jv,1)      &
                                            +ijac(ig,ie,2,2)*aldf(ig,jv,2))     &
                                           *(ijac(ig,ie,2,1)*aldf(ig,iv,1)      &
                                            +ijac(ig,ie,2,2)*aldf(ig,iv,2))))
              ENDDO
              DO iq=1,f%nqty
                integrand(iq,iq,ie,jv,iv)=tmpsum
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ELSE
!-------------------------------------------------------------------------------
!     this version use precomputed test/trial functions
!-------------------------------------------------------------------------------
      ASSOCIATE (f=>field(bl%ibl)%f,alpha=>field(bl%ibl)%f%qab%alf,             &
                 grad_alpha=>field(bl%ibl)%f%qab%aldf,wdetj=>bl%metric%wdetj)
        !$acc parallel present(integrand,alpha,grad_alpha,wdetj,bl,f)           &
        !$acc copyin(fac) async(bl%id) if(bl%on_gpu)
        !$acc loop gang worker
        DO ie=1,bl%nel
          !!$acc cache(wdetj(:,ie))
          !$acc loop vector collapse(2) independent private(tmpsum)
          DO iv=1,f%nbasis
            DO jv=1,f%nbasis
              tmpsum=0._r8
              !$acc loop seq
              DO ig=1,bl%ng
                tmpsum=tmpsum                                                   &
                       +wdetj(ig,ie)*(alpha(ig,jv,ie,1)*alpha(ig,iv,ie,1)       &
                       +fac*SUM(grad_alpha(ig,jv,ie,:)*grad_alpha(ig,iv,ie,:)))
              ENDDO
              DO iq=1,f%nqty
                integrand(iq,iq,ie,jv,iv)=tmpsum
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE diff_op

!-------------------------------------------------------------------------------
! Time-dependent diffusion RHS integrand routine
!-------------------------------------------------------------------------------
  SUBROUTINE diff_rhs(bl,integrand)
    USE local
    USE gblock_mod
    IMPLICIT NONE

    !> block associated with integrand
    CLASS(gblock), INTENT(IN) :: bl
    !> integrand to be computed
    REAL(r8), CONTIGUOUS, INTENT(OUT) :: integrand(:,:,:)

    INTEGER(i4) :: iq,iv,ie,ig
    REAL(r8) :: tmpsum,fac
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1('diff_rhs','diff_rhs',iftn,idepth)
    fac=dt*diff*(1._r8-theta)
    IF (compute_integrand_alpha) THEN
      ASSOCIATE (f=>field(bl%ibl)%f,qfield=>qpfield(bl%ibl)%qpf,                &
                 dqfield=>qpfield(bl%ibl)%qpdf,alpha=>field(bl%ibl)%f%qa%alf,   &
                 aldf=>field(bl%ibl)%f%qa%aldf,wdetj=>bl%metric%wdetj,          &
                 ijac=>bl%metric%ijac)
        !$acc parallel present(integrand,alpha,aldf,qfield,dqfield,wdetj)       &
        !$acc present(f,bl) copyin(fac) async(bl%id) if(bl%on_gpu)
        !$acc loop gang worker
        DO ie=1,bl%nel
          !$acc loop vector collapse(2) independent private(tmpsum)
          DO iv=1,f%nbasis
            DO iq=1,f%nqty
              tmpsum=0._r8
              !$acc loop seq
              DO ig=1,bl%ng
                tmpsum=tmpsum                                                   &
                        +wdetj(ig,ie)*(qfield(ig,ie,1,iq)*alpha(ig,iv)          &
                            -fac*(dqfield(ig,ie,1,iq)*                          &
                                  (ijac(ig,ie,1,1)*aldf(ig,iv,1)                &
                                  +ijac(ig,ie,1,2)*aldf(ig,iv,2))               &
                                  +dqfield(ig,ie,2,iq)*                         &
                                  (ijac(ig,ie,2,1)*aldf(ig,iv,1)                &
                                  +ijac(ig,ie,2,2)*aldf(ig,iv,2))))
              ENDDO
              integrand(iq,ie,iv)=tmpsum
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ELSE
      ASSOCIATE (f=>field(bl%ibl)%f,qfield=>qpfield(bl%ibl)%qpf,                &
                 dqfield=>qpfield(bl%ibl)%qpdf,alpha=>field(bl%ibl)%f%qab%alf,  &
                 grad_test=>field(bl%ibl)%f%qab%aldf,wdetj=>bl%metric%wdetj)
        !$acc parallel present(integrand,alpha,grad_test,qfield,dqfield,wdetj)  &
        !$acc present(f,bl) copyin(fac) async(bl%id) if(bl%on_gpu)
        !$acc loop gang worker
        DO ie=1,bl%nel
          !$acc loop vector collapse(2) independent private(tmpsum)
          DO iv=1,f%nbasis
            DO iq=1,f%nqty
              tmpsum=0._r8
              !$acc loop seq
              DO ig=1,bl%ng
                tmpsum=tmpsum                                                   &
                        +wdetj(ig,ie)*(qfield(ig,ie,1,iq)*alpha(ig,iv,ie,1)     &
                            -fac*SUM(dqfield(ig,ie,:,iq)*grad_test(ig,iv,ie,:)))
              ENDDO
              integrand(iq,ie,iv)=tmpsum
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE diff_rhs

END PROGRAM laplace
