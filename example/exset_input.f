!-------------------------------------------------------------------------------
!! define exset input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module that defines exset input
!-------------------------------------------------------------------------------
MODULE exset_input
  USE local
  IMPLICIT NONE

  PUBLIC

!-------------------------------------------------------------------------------
!===============================================================================
!: namelist exset_input
!===============================================================================
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* field data type, other option is comp.
!-------------------------------------------------------------------------------
  CHARACTER(64) :: field_type="real"
!-------------------------------------------------------------------------------
!> perturbation type, other valid options are sine_waves, bessel_cosine_rect,
! and bessel_cosine_circ.
!-------------------------------------------------------------------------------
  CHARACTER(64) :: init_type="waves"
!-------------------------------------------------------------------------------
!* perturbation x wave index (kx=nx*twopi).
!-------------------------------------------------------------------------------
  REAL(r8) :: nx=1._r8
!-------------------------------------------------------------------------------
!* perturbation y wave index (ky=ny*twopi).
!-------------------------------------------------------------------------------
  REAL(r8) :: ny=1._r8
!-------------------------------------------------------------------------------
!* Bessel function order
!-------------------------------------------------------------------------------
  INTEGER(i4) :: bessel_order=1_i4
!-------------------------------------------------------------------------------
!* initial step number to avoid file duplication while testing.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: istep0=0
!-------------------------------------------------------------------------------
!* number of fields to solve (duplicate problems) for performance tuning.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nqty=1
!-------------------------------------------------------------------------------
!===============================================================================
!: computed quantities
!===============================================================================
!-------------------------------------------------------------------------------
CONTAINS

!-------------------------------------------------------------------------------
!> compute secondary parameters and check input
!-------------------------------------------------------------------------------
  SUBROUTINE exset_compute
    USE pardata_mod
    IMPLICIT NONE

    ! nothing to do
  END SUBROUTINE exset_compute

!-------------------------------------------------------------------------------
!> open and read the namelist input.
!-------------------------------------------------------------------------------
  SUBROUTINE exset_read_namelist(infile)
    USE local
    USE fourier_input_mod
    USE grid_input_mod
    USE read_namelist_mod
    IMPLICIT NONE

    !> input file name
    CHARACTER(*), INTENT(IN) :: infile

    INTEGER(i4) :: read_stat
    CHARACTER(64) :: tempfile
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
    NAMELIST/exset_input/field_type,init_type,nx,ny,bessel_order,istep0,nqty
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   Open and remove comments from input file and put into temporary file.
!-------------------------------------------------------------------------------
    tempfile='temp'//ADJUSTL(infile)
    CALL rmcomment(infile,tempfile)
    OPEN(UNIT=in_unit,FILE=tempfile,STATUS='OLD',POSITION='REWIND')
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
    CALL position_at_namelist('exset_input', read_stat)
    IF (read_stat==0) THEN
      READ(UNIT=in_unit,NML=exset_input,IOSTAT=read_stat)
      IF (read_stat/=0) THEN
        CALL print_namelist_error('exset_input')
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   close and delete temporary input file.
!-------------------------------------------------------------------------------
    CLOSE(in_unit,STATUS='DELETE')
!-------------------------------------------------------------------------------
!   compute secondary parameters.
!-------------------------------------------------------------------------------
    CALL exset_compute
!-------------------------------------------------------------------------------
!   read other namelists.
!-------------------------------------------------------------------------------
    CALL fourier_in%read_namelist(infile)
    CALL grid_in%read_namelist(infile)
  END SUBROUTINE exset_read_namelist

!-------------------------------------------------------------------------------
!> read h5 namelist from dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE exset_h5read
    USE io
    IMPLICIT NONE

    INTEGER(HID_T) :: gid

    CALL open_group(rootgid,"exset",gid,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL read_attribute(gid,"field_type",field_type,h5in,h5err)
    CALL read_attribute(gid,"init_type",init_type,h5in,h5err)
    CALL read_attribute(gid,"nx",nx,h5in,h5err)
    CALL read_attribute(gid,"ny",ny,h5in,h5err)
    CALL read_attribute(gid,"bessel_order",bessel_order,h5in,h5err)
    CALL read_attribute(gid,"istep0",istep0,h5in,h5err)
    CALL read_attribute(gid,"nqty",nqty,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL close_group("exset",gid,h5err)
  END SUBROUTINE exset_h5read

!-------------------------------------------------------------------------------
!> write h5 namelist to dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE exset_h5write
    USE io
    IMPLICIT NONE

    INTEGER(HID_T) :: gid

    CALL make_group(rootgid,"exset",gid,h5in,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL write_attribute(gid,"field_type",field_type,h5in,h5err)
    CALL write_attribute(gid,"init_type",init_type,h5in,h5err)
    CALL write_attribute(gid,"nx",nx,h5in,h5err)
    CALL write_attribute(gid,"ny",ny,h5in,h5err)
    CALL write_attribute(gid,"bessel_order",bessel_order,h5in,h5err)
    CALL write_attribute(gid,"istep0",istep0,h5in,h5err)
    CALL write_attribute(gid,"nqty",nqty,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL close_group("exset",gid,h5err)
  END SUBROUTINE exset_h5write

!-------------------------------------------------------------------------------
!> broadcast info read by proc 0 out of nimrod.in (namelist reads) to all
! processors. every quantity in input module is broadcast in case it was read.
!-------------------------------------------------------------------------------
  SUBROUTINE exset_bcast_input
    USE fourier_input_mod
    USE grid_input_mod
    USE pardata_mod
    IMPLICIT NONE

    INTEGER(i4) :: nn
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   exset_input
!-------------------------------------------------------------------------------
    nn=LEN(field_type)
    CALL par%all_bcast(field_type,nn,0)
    nn=LEN(init_type)
    CALL par%all_bcast(init_type,nn,0)
    CALL par%all_bcast(nx,0)
    CALL par%all_bcast(ny,0)
    CALL par%all_bcast(bessel_order,0)
    CALL par%all_bcast(istep0,0)
    CALL par%all_bcast(nqty,0)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
    CALL exset_compute
!-------------------------------------------------------------------------------
!   broadcast other namelists.
!-------------------------------------------------------------------------------
    CALL fourier_in%bcast_input
    CALL grid_in%bcast_input
  END SUBROUTINE exset_bcast_input

END MODULE exset_input
