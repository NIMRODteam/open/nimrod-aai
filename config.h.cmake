#ifndef CONFIG_H
#define CONFIG_H

#cmakedefine CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}
#cmakedefine CMAKE_Fortran_COMPILER_ID ${CMAKE_Fortran_COMPILER_ID}
#cmakedefine CMAKE_Fortran_FLAGS_RELEASE ${CMAKE_Fortran_FLAGS_RELEASE}
#cmakedefine CMAKE_Fortran_FLAGS_RELWITHDEBINFO ${CMAKE_Fortran_FLAGS_RELWITHDEBINFO}
#cmakedefine CMAKE_Fortran_FLAGS_DEBUG ${CMAKE_Fortran_FLAGS_DEBUG}
#cmakedefine DEBUG
#cmakedefine HAVE_ACC_BLAS
#cmakedefine HAVE_BLAS
#cmakedefine HAVE_CUBLAS
#cmakedefine HAVE_FFTW3
#cmakedefine HAVE_HDF5
#cmakedefine HAVE_LAPACK
#cmakedefine HAVE_MPI
#cmakedefine HAVE_MPI_F08
#cmakedefine HAVE_OPENACC
#cmakedefine HAVE_SUPERLU_DIST
#cmakedefine HAVE_SUPERLU
#cmakedefine MPI_THREAD_FUNNELED
#cmakedefine NERSC_PERLMUTTER
#cmakedefine NIMROD_COMPILE_FLAGS ${NIMROD_COMPILE_FLAGS}
#cmakedefine NVTX_PROFILE
#cmakedefine OBJ_MEM_PROF
#cmakedefine OPENACC_AUTOCOMPARE
#cmakedefine PROJECT_URL ${PROJECT_URL}
#cmakedefine PROJECT_VERSION ${PROJECT_VERSION}
#cmakedefine TIME_LEVEL1
#cmakedefine TIME_LEVEL2
#cmakedefine __cray
#cmakedefine __flang
#cmakedefine __ifort
#cmakedefine __gfortran
#cmakedefine __nvhpc

#endif
